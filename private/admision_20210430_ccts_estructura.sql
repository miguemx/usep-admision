CREATE TABLE `usep_aplicativos`.`cat_ccts` ( 
    `cct_cct` VARCHAR(11) NOT NULL , 
    `cct_nombre` VARCHAR(250) NOT NULL , 
    `cct_tipo_sep` VARCHAR(100) NOT NULL , 
    `cct_calle` VARCHAR(250) NOT NULL , 
    `cct_num` VARCHAR(20) NOT NULL , 
    `cct_estado` VARCHAR(60) NOT NULL , 
    `cct_municipio` VARCHAR(120) NOT NULL , 
    `cct_localidad` VARCHAR(120) NOT NULL , 
    `cct_colonia` VARCHAR(250) NOT NULL , 
    `cct_cp` VARCHAR(5) NOT NULL , 
    `cct_tipo` VARCHAR(15) NOT NULL , 
    `cct_nivel` VARCHAR(50) NOT NULL , 
    PRIMARY KEY (`cct_cct`)
) ENGINE = InnoDB;