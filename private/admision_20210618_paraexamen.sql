SELECT
    view_aspirantes_full.aspirante_ap_paterno,
    view_aspirantes_full.aspirante_ap_materno,
    view_aspirantes_full.aspirante_nombre,
    view_aspirantes_full.aspirante_curp,
    view_aspirantes_full.aspirante_correo,
    view_aspirantes_full.aspirante_id,
    view_aspirantes_full.carrera_clave,
    view_aspirantes_full.carrera_nombre,
    view_aspirantes_full.aspirante_telefono,
    '0' AS TELOP,
    view_aspirantes_full.procedencia_cct,
    view_aspirantes_full.procedencia_nombre,
    view_aspirantes_full.salud_discapacidad,
    prein_aspirantes.aspirante_examen_fecha,
    prein_aspirantes.aspirante_examen_hora

FROM
    view_aspirantes_full INNER JOIN prein_aspirantes on view_aspirantes_full.aspirante_id=prein_aspirantes.aspirante_id

WHERE 
    proceso_exito ='1'