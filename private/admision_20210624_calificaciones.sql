ALTER TABLE `prein_aspirantes` ADD `aspirante_calif_p1` INT NULL COMMENT 'calificacion del proveedor 1' AFTER `deleted_at`, ADD `aspirante_calif_p2` INT NULL COMMENT 'calificacion del proveedor 2' AFTER `aspirante_calif_p1`, ADD `aspirante_calif_examen` INT NULL COMMENT 'calificacion del examen' AFTER `aspirante_calif_p2`, ADD `aspirante_calif_promedio` INT NULL COMMENT 'calificacion del promedio' AFTER `aspirante_calif_examen`, ADD `aspirante_calif_socio` INT NULL COMMENT 'calificacion socio' AFTER `aspirante_calif_promedio`, ADD `aspirante_calif_final` INT NULL COMMENT 'calificacion fnal se muestra el resultado' AFTER `aspirante_calif_socio`;

ALTER TABLE `prein_aspirantes` ADD `aspirante_res_orden` INT NULL AFTER `aspirante_calif_final`;
ALTER TABLE `prein_aspirantes` ADD `aspirante_aceptado` VARCHAR(1) NULL COMMENT 'bandera que indica si el aspirante es aceptado o no' AFTER `aspirante_res_orden`;


ALTER TABLE `prein_aspirantes` ADD `aspirante_insc_fecha` DATE NULL COMMENT 'fecha de inscripcion' AFTER `aspirante_aceptado`, ADD `aspirante_insc_hora` TIME NULL COMMENT 'hora de inscripcion' AFTER `aspirante_insc_fecha`;