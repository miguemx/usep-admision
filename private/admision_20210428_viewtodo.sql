CREATE VIEW view_aspirantes_full AS

SELECT 
	aspirante_id,
    aspirante_carrera,
    aspirante_ap_paterno,
    aspirante_ap_materno,
    aspirante_nombre,
    aspirante_curp,
    aspirante_correo,
    aspirante_telefono,
    aspirante_calle,
    aspirante_numero,
    aspirante_numero_interior,
    aspirante_cp,
    aspirante_colonia,
    aspirante_municipio,
    aspirante_estado,
    aspirante_pais,
    aspirante_nacimiento_fecha,
    aspirante_nacimiento_municipio,
    aspirante_nacimiento_estado,
    aspirante_nacimiento_pais,
    aspirante_enviado,
    
    carrera_nombre,
    carrera_numero,
    carrera_clave,
    
    socio_estado_civil,
    socio_ingreso,
    socio_hijos,
    socio_dependientes,
    socio_mp_soltero,
    socio_personas_casa,
    socio_vive_en,
    socio_radicar,
    socio_trabaja_salud,
    socio_comunidad_indigena,
    socio_situacion_vulnerable,
    socio_vulnerable_cual,
    socio_admision_previo,
    socio_admision_actual,
    socio_tiene_desktop,
    socio_tiene_webcam, 
    socio_tiene_laptop,
    socio_tiene_internet,
    socio_tiene_tablet,
    socio_tiene_smartphone,
    socio_beca,
    socio_enviado,
    socio_examen_online,
    socio_conseguir_compu,
    
    salud_tipo_sangre,
    salud_padecimientos,
    salud_discapacidad,
    salud_certificado_discapacidad,
    salud_usa_lentes,
    salud_tiene_protesis,
    salud_servicio_medico,
    salud_enviado,
    
    procedencia_aspirante,
    procedencia_tipo,
    procedencia_cct,
    procedencia_nombre,
    procedencia_calle,
    procedencia_num,
    procedencia_cp,
    procedencia_colonia,
    procedencia_municipio,
    procedencia_estado,
    procedencia_pais,
    procedencia_fecha_conclusion,
    procedencia_promedio,
    procedencia_enviado,
    
    emergencia_aspirante,
    emergencia_nombre,
    emergencia_parentesco,
    emergencia_telefono,
    emergencia_correo,
    emergencia_enviado,
    
    proceso_registro,
    proceso_validacion,
    proceso_captura,
    proceso_documentos,
    proceso_revision,
    proceso_exito,
    proceso_declinado,
    proceso_rechazado,
    
    prein_aspirantes.created_at as creacion,

    aspirante_aceptado
   
FROM
	prein_aspirantes
    INNER JOIN prein_carreras ON aspirante_carrera = carrera_id
    INNER JOIN prein_socioeconomicos ON socio_aspirante = aspirante_id
    INNER JOIN prein_aspirante_salud ON salud_aspirante = aspirante_id
    INNER JOIN prein_procedencia ON procedencia_aspirante = aspirante_id
    INNER JOIN prein_contactos_emergencia ON emergencia_aspirante = aspirante_id
    INNER JOIN prein_proceso ON proceso_aspirante = aspirante_id

WHERE prein_aspirantes.deleted_at IS NULL;