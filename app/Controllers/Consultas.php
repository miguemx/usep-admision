<?php
namespace App\Controllers;

use App\Models\CctsModel;
use App\Models\LocalidadesModel;

class Consultas extends BaseController {

    protected $cctsModel;

    public function __construct() {
        $this->cctsModel = new CctsModel();
    }

    public function cctEstados() {
        $this->cctsModel->getEstados();
    }

    public function cctMunicipios( $estado ) {

    }

    public function getEstados() {
        $localidadesModel = new LocalidadesModel();
        $estados = $localidadesModel->getEstados();
        var_dump( $estados );
    }

    public function getMunicipios() {
        $estado = $this->request->getPost( 'estado' );
        $localidadesModel = new LocalidadesModel();
        $municipios = $localidadesModel->getMunicipiosPorNombreEstado( $estado );
        $this->response->setHeader('Content-Type', 'application/json');
        echo json_encode( $municipios );
    }

    public function getEstadosCct() {
        $localidadesModel = new LocalidadesModel();
        $estados = $localidadesModel->getEstadosCct();
        $this->response->setHeader('Content-Type', 'application/json');
        echo json_encode( $municipios );
    }
    public function getMunicipiosCct() {
        $localidadesModel = new LocalidadesModel();
        $estado = $this->request->getPost( 'estado' );
        $municipios = $localidadesModel->getMunicipiosCct( $estado );
        $this->response->setHeader('Content-Type', 'application/json');
        echo json_encode( $municipios );
    }

    public function getCcts() {
        $localidadesModel = new LocalidadesModel();
        $municipio = $this->request->getPost( 'municipio' );
        $ccts = $localidadesModel->getCcts( $municipio );
        $this->response->setHeader('Content-Type', 'application/json');
        echo json_encode( $ccts );
    }
}