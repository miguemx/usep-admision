<?php

namespace App\Controllers;

use CodeIgniter\Controller;
use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use Psr\Log\LoggerInterface;

use App\Models\AspirantesModel;
use App\Models\ContactosEmergenciaModel;
use App\Models\ProcedenciasModel;
use App\Models\SaludModel;
use App\Models\SocioeconomicosModel;
use App\Models\ProcesosModel;

/**
 * Class BaseController
 *
 * BaseController provides a convenient place for loading components
 * and performing functions that are needed by all your controllers.
 * Extend this class in any new controllers:
 *     class Home extends BaseController
 *
 * For security be sure to declare any new methods as protected or private.
 */

class BaseController extends Controller
{
	/**
	 * An array of helpers to be loaded automatically upon
	 * class instantiation. These helpers will be available
	 * to all other controllers that extend BaseController.
	 *
	 * @var array
	 */
	protected $helpers = [];

	protected $errorMessage = '';

	protected $Validacion;

	protected $sesion;

	private $cifradoKey = '2f4865152224dc5955b8844c852f224e';
	private $cifradoAlgoritmo = 'OpenSSL';

	// normal para Webmaster
	private $mailProtocol = 'smtp';
	private $mailHost = 'smtp.gmail.com';
	private $mailUser = 'webmaster@usep.mx';
	private $mailPass = 'rokoxashvkehmvnj';  
	private $mailPort = '587';
	private $mailCrypto = 'tls';

	/**
	 * Constructor.
	 *
	 * @param RequestInterface  $request
	 * @param ResponseInterface $response
	 * @param LoggerInterface   $logger
	 */
	public function initController(RequestInterface $request, ResponseInterface $response, LoggerInterface $logger)
	{
		// Do Not Edit This Line
		parent::initController($request, $response, $logger);

		//--------------------------------------------------------------------
		// Preload any models, libraries, etc, here.
		//--------------------------------------------------------------------
		// E.g.: $this->session = \Config\Services::session();
		$this->sesion = \Config\Services::session();
		$this->Validacion = \Config\Services::validation();
	}

	/**
	 * invoca la libreria de validacion y regresa el resultado de dicha validacion
	 * @param data los datos a validar en un array asociativo
	 * @param regla una cadena con la regla de validacion a utilizar, registrada en la libreria de validacion
	 * @return true verdadero si no existe ningun error
	 * @return result arreglo con los mensajes de error
	 */
	protected function valida($data, $regla) {
		if ( $this->Validacion->run( $data, $regla ) ) { 
			return true;
		}
		else {
			$errors = array();
			foreach ( $this->Validacion->getErrors() as $error ) {
				$errors[] = $error;
			}
			return $errors;
		}
	}

	/**
	 * cifra una cadena 
	 * @param cadena la cadena a cifrar
	 * @return cifrado la cadena cifrada
	 */
	protected function cifrar( $cadena ) {
		$config         = new \Config\Encryption();
		$config->key    = $this->cifradoKey;
		$config->driver = $this->cifradoAlgoritmo;

		$encrypter = \Config\Services::encrypter($config);
		$ciphertext = $encrypter->encrypt($cadena);
		$cifrado = base64_encode( $ciphertext );
		$cifrado = str_replace( '+', '_MAS_', $cifrado );
		$cifrado = str_replace( '/', '_DIA_', $cifrado );
		return $cifrado;
	}

	/**
	 * descifra una cadena
	 * @param cifrado la cadena cifrada
	 * @return cadena la cadena descifrada
	 * @return null en caso de error en el descifrado
	 */
	protected function descifrar ( $cifrado ) {
		$cadena = null;

		$cifrado = str_replace( '_MAS_', '+', $cifrado );
		$cifrado = str_replace( '_DIA_', '/', $cifrado );

		$config         = new \Config\Encryption();
		$config->key    = $this->cifradoKey;
		$config->driver = $this->cifradoAlgoritmo;
		$encrypter = \Config\Services::encrypter($config);

		try {
			$ciphertext = base64_decode( $cifrado );
			$cadena = $encrypter->decrypt($ciphertext);
		}
		catch(\CodeIgniter\Encryption\Exceptions\EncryptionException $ex) {
			$this->errorMessage = 'No se puede encontrar descifrar el valor. '.$ex->getMessage();	
		}
		return $cadena;
	}

	/**
	 * envia un correo electrónico a la dirección especificada con el mensaje proporcionado
	 * @param destinatario la dirección de correo del destinatario
	 * @param mensaje (opcional) el mensaje a enviar; si no se envía mensaje solo se envía vacío
	 */
	protected function enviaEmail( $destinatario, $mensaje = '', $asunto = '(Vacío)' ) {
		$email = \Config\Services::email();

		$config['mailType'] = 'html';
		$config['protocol'] = $this->mailProtocol;
		$config['SMTPHost'] = $this->mailHost;
		$config['SMTPUser'] = $this->mailUser;
		$config['SMTPPass'] = $this->mailPass;
		$config['SMTPPort'] = $this->mailPort;
		$config['SMTPCrypto'] = $this->mailCrypto;
		$email->initialize($config);
		
		$email->setFrom('webmaster@usep.mx', 'Admisión USEP 2021');
		$email->setTo( $destinatario );
		$email->setBCC('pruebas@usep.mx');

		$email->setSubject( $asunto );
		$email->setMessage( $mensaje );

		$email->send(false);
	}

	/**
	 * obtiene el progreso general del aspirante y sus respectivos pasos en la captura
	 */
	protected function getProgreso() {
        $progreso = [ 'global' => [ 
            'registro' => '0', 'validacion' => '0', 'captura' => '0', 'revision' => '0', 'finalizado' => '', 'rechazado'=>'' ], 
            'captura' => [ 'generales' => '0', 'socioeconomicos' => '0', 'salud' => '0', 'procedencia' => '0', 'emergencia' => '0' ] 
        ];
        $matricula = $this->sesion->get('id');
		$procesosModel = new ProcesosModel();
        $proceso = $procesosModel->find( $matricula );
        if ( !is_null($proceso) ) {
            $progreso['global']['registro'] = $proceso->registro;
            $progreso['global']['validacion'] = $proceso->validacion;
            $progreso['global']['captura'] = $proceso->captura;
			$progreso['global']['documentos'] = $proceso->documentos;
            $progreso['global']['revision'] = $proceso->revision;
            $progreso['global']['finalizado'] = ( $proceso->exito =='1' || $proceso->declinado == '1' ) ? '1': '0';
            $progreso['global']['status'] = ($proceso->exito == '1') ? 'CORRECTO' : 'DECLINADO' ;
			$progreso['global']['exito'] = $proceso->exito == '1';
			$progreso['global']['declinado'] = $proceso->declinado == '1';
			$progreso['global']['rechazado'] = $proceso->rechazado;
        }
        $aspirantesModel = new AspirantesModel();
        $socioeconomicosModel = new SocioeconomicosModel();
        $saludModel = new SaludModel();
        $procedenciaModel = new ProcedenciasModel();
        $emergenciaModel = new ContactosEmergenciaModel();
        $generales = $aspirantesModel->find( $matricula );
        $socio = $socioeconomicosModel->find( $matricula );
        $salud = $saludModel->find( $matricula );
        $procedencia = $procedenciaModel->find( $matricula );
        $emergencia = $emergenciaModel->find( $matricula );
        $progreso['captura']['generales'] = $generales->enviado;
        $progreso['captura']['socioeconomicos'] = $socio->enviado;
        $progreso['captura']['salud'] = $salud->enviado;
        $progreso['captura']['procedencia'] = $procedencia->enviado;
        $progreso['captura']['emergencia'] = $emergencia->enviado;

        return $progreso;
    }

}
