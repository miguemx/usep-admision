<?php

namespace App\Controllers;

use App\Entities\Intento;
use App\Entities\Aspirante;
use App\Entities\Carrera;
use App\Entities\Proceso;
use App\Entities\ContactoEmergencia;
use App\Entities\Procedencia;
use App\Entities\Salud;
use App\Entities\Socioeconomico;
use App\Models\IntentosModel;
use App\Models\AspirantesModel;
use App\Models\CarrerasModel;
use App\Models\ProcesosModel;
use App\Models\ContactosEmergenciaModel;
use App\Models\ProcedenciasModel;
use App\Models\SaludModel;
use App\Models\SocioeconomicosModel;

class Home extends BaseController
{
	/**
	 * muestra los formularios de registro y de inicio de sesion
	 */
	public function index()
	{
		$data = [
			'apPaterno' => '', 'apMaterno' => '', 'nombre' => '', 'curp' => '', 'telefono' => '', 'correo' => '',
		];
		if ( $flashData = $this->sesion->getFlashdata('error') ) {
			$data['sesserror'] = $flashData;
		}
		return view('home/bienvenida', $data);
	}

	/**
	 * ejecuta el registro de un nuevo intento de aspirante
	 */
	public function registro() {
		$data = array();
		$datos = $this->request->getPost();
		$validacion = $this->valida( $datos, 'registro' );
		$data = $datos;
		if ( $validacion === true ) {
			$intentosModel = new IntentosModel();
			$intento = $intentosModel->findByCurp( $datos['curp'] );
			if ( is_null($intento) ) {
				$datos['nombre'] = mb_strtoupper( $datos['nombre'] );
				$datos['apPaterno'] = mb_strtoupper( $datos['apPaterno'] );
				$datos['apMaterno'] = mb_strtoupper( $datos['apMaterno'] );
				$datos['curp'] = mb_strtoupper( $datos['curp'] );
				$intento = new Intento( $datos );
				$intentosModel->insert( $intento );
				$dataLink = $this->cifrar( time().'-'.$intento->curp );
				$link = base_url('Home/Validacion').'/'.$dataLink;
				$intento->link = $link;
				$this->enviaEmail( $intento->correo, $this->getMensajeCorreo( $intento ), 'Admisión USEP 2021 - Validación de cuenta.' );
				return view('home/mensaje', $data);
			}
			$data['errors'][0] = 'La CURP ya se encuentra registrada. Por favor verifica tu correo para encontrar el enlace de validación. Si no ves el correo verifica también tu carpeta de correo no deseado o spam. En caso de que no hayas realizado el registro, por favor comunícate con nosotros para verificar tu caso.';
		}
		else {
			$data['errors'] = $validacion;
		}
		return view('home/bienvenidasimple', $data);
	}

	/**
	 * realiza una validacion del token
	 * @param regid el token enviado para validar
	 */
	public function validacion($regid) {
		$data = [];
		$dataLink = $this->descifrar( $regid );
		if ( !is_null($dataLink) ) {
			$dataObtained = explode( '-', $dataLink );
			$dif = time() - $dataObtained[0];
			if ( $dif < DAY ) {
				$aspirante = $this->creaAspirante( $dataObtained[1] );
				if ( $aspirante ) {
					$this->enviaEmail( $aspirante->correo, $this->getMensajeFolio( $aspirante ), 'Admisión USEP 2021 - Datos de acceso para el portal de aspirantes.' );
					$data['valido'] = true;
				}
				else {
					$data['error'] = $this->errorMessage;	
				}
			}
			else {
				$data['error'] = 'El enlace ha expirado.';
			}
		}
		else {
			$data['error'] = $this->errorMessage;
		}
		return view( 'home/validacion', $data );
	}

	/**
	 * valida los datos para iniciar sesión y la concede o deniega
	 * redirigiendo al portal de perfil de aspirante, o al inicio
	 */
	public function login() {
		$curp = $this->request->getPost( 'logincurp' );
		$matricula = $this->request->getPost( 'loginfolio' );
		$aspirantesModel = new AspirantesModel();
		$aspirante = $aspirantesModel->find( $matricula );
		if ( !is_null($aspirante) ) {
			if ( $aspirante->curp == $curp ) {
				$this->sesion->set( 'curp' , $aspirante->curp );
				$this->sesion->set( 'id' , $aspirante->id );
				$this->sesion->set( 'nombre' , $aspirante->nombre );
				return redirect()->to( base_url('Perfil') );
			}
		}
		$this->sesion->setFlashdata( 'error', 'La CURP proporcionada y el folio no coinciden o no se encuentran. Verifica tus datos por favor.' );
		return redirect()->to( base_url() );
	}

	/**
	 * destruye la sesión y dirige a la página de bienvenida
	 */
	public function logout() {
		$this->sesion->destroy();
		return redirect()->to( base_url() );

	}


	public function recuperacion($tipo) {
		$data = ['curp' => '', 'apPaterno' => '', 'telefono' => '', 'action' =>'' ];
		$intentosModel = new IntentosModel();
		$aspirantesModel = new AspirantesModel();
		$intento = null;
		$aspirante = null;
		$post = $this->request->getPost('btbus');
		
		if ( !is_null($post) ) {
			$intento = $intentosModel->findByCurp( $this->request->getPost('curp') );
			$aspirante = $aspirantesModel->findByCurp( $this->request->getPost('curp') );
		}
		if ( $tipo == '1' ) { 
			$data['action'] = base_url('Home/Recuperacion/1');
			if ( !is_null($intento) ) {
				$data['curp'] = $intento->curp;
				if ( $intento->apPaterno == mb_strtoupper($this->request->getPost('apPaterno')) && $intento->telefono == $this->request->getPost('telefono') ) {
					$dataLink = $this->cifrar( time().'-'.$intento->curp );
					$data['enlace'] = base_url('Home/Validacion').'/'.$dataLink;
				}
				else {
					$data['error'] = 'Los datos que proporcionaste no coinciden.';
				}
			}
			else {
				$data['error'] = 'No se han encontrado los datos que proporcionaste.';
			}
		}
		else if ( $tipo == '2' ) {
			$data['action'] = base_url('Home/Recuperacion/2');
			if ( !is_null($aspirante) ) {
				$data['curp'] = $aspirante->curp;
				if ( $aspirante->apPaterno == mb_strtoupper($this->request->getPost('apPaterno')) && $aspirante->telefono == $this->request->getPost('telefono') ) {
					$data['dfolio'] = $aspirante->id;
				}
				else {
					$data['error'] = 'Los datos que proporcionaste no coinciden.';
				}
			}
			else {
				$data['error'] = 'No se han encontrado los datos que proporcionaste.';
			}
		} 
		else {
			$data['error'] = 'No se puede realizar la búsqueda.';
		}
		if ( is_null($post) ) unset( $data['error'] );
		return view('home/recuperacion', $data);
	}

	/**
	 * obtiene la plantilla de mensaje de correo para el registro
	 * @param datos el objeto Intento con los datos que van en el correo
	 * @return correo el HTML del cuerpo del correo
	 */
	private function getMensajeCorreo($datos) {
		$correo = file_get_contents( APPPATH.'/Views/home/mailregistro.php' );
		$correo = str_replace( '{{nombre}}', $datos->nombre, $correo );
		$correo = str_replace( '{{liga}}', $datos->link, $correo );
		return $correo;
	}

	/**
	 * obtiene la plantilla de mensaje de correo para el registro
	 * @param datos el objeto Aspirante con los datos que van en el correo
	 * @return correo el HTML del cuerpo del correo
	 */
	private function getMensajeFolio($datos) {
		$correo = file_get_contents( APPPATH.'/Views/home/mailvalidacion.php' );
		$correo = str_replace( '{{nombre}}', $datos->nombre, $correo );
		$correo = str_replace( '{{liga}}', base_url(), $correo );
		$correo = str_replace( '{{curp}}', $datos->curp, $correo );
		$correo = str_replace( '{{matricula}}', $datos->id, $correo );
		return $correo;
	}

	/**
	 * realiza la validación del registro, creando un nuevo registro de aspirante y su respectivo proceso
	 * @param curp la curp que será buscada para validar y crear el registro
	 * @return aspirante El aspirante generado
	 * @return false si no se puede crear el nuevo registro
	 */
	private function creaAspirante($curp) {
		$aspirantesModel = new AspirantesModel();
		$aspirante = $aspirantesModel->findByCurp( $curp );
		$this->errorMessage = 'El aspirante ya ha sido registrado. Por favor accede a tu perfil con los datos que se proporcionaron en tu correo electrónico.';
		if ( is_null($aspirante) ) {  // no se encuentra y se puede crear sin problema
			$intentosModel = new IntentosModel();
			$intento = $intentosModel->findByCurp( $curp );
			$this->errorMessage = 'No se ha encontrado un registro previo. Por favor llene el formulario de registr para poder crear su cuenta.';
			if ( !is_null($intento) ) {
				try {
					$carrerasModel = new CarrerasModel();
					$carrera = $carrerasModel->find( $intento->carrera );
					$aspirantesModel = new AspirantesModel();
					$aspirante = new Aspirante();
					$aspirante->id = $aspirantesModel->calculaSiguienteMatricula( $carrera );
					$aspirante->carrera = $intento->carrera;
					$aspirante->apPaterno = mb_strtoupper( $intento->apPaterno );
					$aspirante->apMaterno = mb_strtoupper( $intento->apMaterno );
					$aspirante->nombre = mb_strtoupper( $intento->nombre );
					$aspirante->curp = mb_strtoupper( $intento->curp );
					$aspirante->correo = $intento->correo;
					$aspirante->telefono = $intento->telefono;
					$aspirantesModel->insert( $aspirante );
					$this->creaSecciones( $aspirante->id );
					return $aspirante;
				}
				catch ( \Exception $ex ) {
					$this->errorMessage = 'No se pudo completar tu registro. Por favor vuelve a intentar más tarde o ponte en contacto con nosotros. '.$ex->getMessage();
				}
			}
		}
		return false;
	}

	/**
	 * crea el registro inicial de los datos que debe llenar cada aspirate
	 * @param matricula el ID o matricula del aspirante
	 */
	private function creaSecciones( $matricula ) {
		$procesosModel = new ProcesosModel();
		$proceso = new Proceso();
		$proceso->aspirante = $matricula;
		$proceso->registro = '1';
		$proceso->validacion = '1';
		$procesosModel->insert( $proceso );

		$contactosModel = new ContactosEmergenciaModel();
		$contacto = new ContactoEmergencia();
		$contacto->aspirante = $matricula;
		$contacto->telefono = '';
		$contactosModel->insert( $contacto );

		$procedenciaModel = new ProcedenciasModel();
		$procedencia = new Procedencia();
		$procedencia->aspirante = $matricula;
		$procedenciaModel->insert( $procedencia );

		$saludModel = new SaludModel();
		$salud = new Salud();
		$salud->aspirante = $matricula;
		$saludModel->insert( $salud );

		$socioeconomicosModel = new SocioeconomicosModel();
		$socioeconomico = new Socioeconomico();
		$socioeconomico->aspirante = $matricula;
		$socioeconomico->comunidadIndigena = '0';
		$socioeconomicosModel->insert( $socioeconomico );
	}
}
