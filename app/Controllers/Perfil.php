<?php
namespace App\Controllers;

use App\Models\AspirantesModel;
use App\Models\ContactosEmergenciaModel;
use App\Models\ProcedenciasModel;
use App\Models\SaludModel;
use App\Models\SocioeconomicosModel;
use App\Models\ProcesosModel;
use App\Models\LocalidadesModel;
use App\Models\DocumentosModel;
use App\Models\CarrerasModel;

use App\Entities\Aspirante;
use App\Entities\ContactoEmergencia;
use App\Entities\Procedencia;
use App\Entities\Salud;
use App\Entities\Socioeconomico;

class Perfil extends BaseController {

    protected $erroresCaptura = array();
    protected $procesosModel;

    public function __construct() {
        $this->procesosModel = new ProcesosModel();
    }

    /**
     * ejecuta un filtro de renderizado para generar la vista de los aspirantes de acuerdo a su status
     * @param view la vista a renderizar de la petición original
     * @param data los datos originales para renderizar la vista
     * @return view () el View a renderizar
     */
    private function selectView( $view, $data ) {
        if ( $data['progreso']['global']['exito'] === true ) {
            return $this->selectCarta( $data );
        }
        else {
            $documentosModel = new DocumentosModel();
            $documentos = $documentosModel->getArchivos( $this->sesion->get('id') );
            $data['captura'] = '';
            $data['promedio'] = '';
            $procedenciaModel = new ProcedenciasModel();
            $procedencia = $procedenciaModel->find( $this->sesion->get('id') );
            $data['observaciones'] = [];
            foreach( $documentos as $documento ) { 
                if ( $documento->comentarios ) {
                    $data['observaciones'][] = '<strong>'.$documento->tipoText.'</strong>: '.str_replace( "<br />", '.', ucfirst( nl2br($documento->comentarios) ) );
                }
            }
            if ( $procedencia ) {
                if ( $procedencia->promedio < 8 ) {
                    $data['promedio'] = $procedencia->promedio;
                }
            }

            if  ( $data['progreso']['global']['documentos'] == '0' ) {
                $data['observaciones'][] = 'Usted no concluyó la etapa de carga/corrección de documentos.';
            }

            if  ( $data['progreso']['global']['captura'] == '0' ) {
                $data['captura'] = 'Usted no concluyó el registro de su información.';
            }
            return view('perfil/rechazado', $data);
        }
    }

    private function selectCarta( $data ) {
        $aspirantesModel = new AspirantesModel();
        $aspirante = $aspirantesModel->find( $this->sesion->get('id') );

        // QUITAR PARA LA PUBLICACION
        // if ( $aspirante->id != '210200006' ) {
        //     return view('perfil/aceptado', $data);
        // }

        if ( !is_null($aspirante) ) {
            $data['aceptado'] = $aspirante->aceptado;
            $data['puntaje'] = $aspirante->califFinal;
            $data['inscFecha'] = $aspirante->inscFecha;
            $data['inscHora'] = $aspirante->inscHora;
            $carrerasModel = new CarrerasModel();
            $carrera = $carrerasModel->find( $aspirante->carrera );
            $data['carrera'] = $carrera->nombre;
            return view('perfil/resultado', $data);
        }
        throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
    }

    /**
     * genera la vista de los datos generales del aspirante y guarda los datos cuando se recibe un post
     */
    public function index() {
        $guardar = false;
        $guardado = false;
        if( !is_null( $this->request->getPost('gf') ) || !is_null( $this->request->getPost('ged') ) ) {
            $guardar = true;
            $guardado = $this->guardaGenerales();
        }
        $data = $this->buscaGenerales();
        if ( $guardado ) {
            if( !is_null($this->request->getPost('gf')) ) { // se esta finalizando la seccion
                return redirect()->to( base_url('Perfil/Socioeconomicos') );
            }
        }
        else {
            if ( $guardar ) {
                $data['errors'] = $this->erroresCaptura;
            }
        }
        $data['menu'] = 'aspirante';
        $data['progreso'] = $this->getProgreso();
        if ( $guardar ) $data['guardado'] = true;
        $localidadesModel = new LocalidadesModel();
        $data['estados'] = $localidadesModel->getEstados();
        return $this->selectView( 'perfil/escritorio', $data );
    }

    /**
     * genera la vista de los datos socioeconomicos del aspirante y guarda los datos cuando se recibe un post
     */
    public function socioeconomicos() {
        $guardar = false;
        $guardado = false;
        if( !is_null( $this->request->getPost('gf') ) || !is_null( $this->request->getPost('ged') ) ) {
            $guardar = true;
            $guardado = $this->guardaSocioeconomicos();
        }
        $data = $this->buscaSocioeconomicos();
        if ( $guardado ) {
            if( !is_null($this->request->getPost('gf')) ) { // se esta finalizando la seccion
                return redirect()->to( base_url('Perfil/Salud') );
            }
        }
        else {
            if ( $guardar ) {
                $data['errors'] = $this->erroresCaptura;
            }
        }
        $data['menu'] = 'aspirante';
        $data['progreso'] = $this->getProgreso();
        if ( $guardar ) $data['guardado'] = true;
        return $this->selectView( 'perfil/socioeconomico', $data );
    }

    /**
     * genera la vista de los datos de salud del aspirante y guarda los datos cuando se recibe un post
     */
    public function salud() {
        $guardar = false;
        $guardado = false;
        if( !is_null( $this->request->getPost('gf') ) || !is_null( $this->request->getPost('ged') ) ) {
            $guardar = true;
            $guardado = $this->guardaSalud();
        }
        $data = $this->buscaSalud();
        if ( $guardado ) {
            if( !is_null($this->request->getPost('gf')) ) { // se esta finalizando la seccion
                return redirect()->to( base_url('Perfil/Procedencia') );
            }
        }
        else {
            if ( $guardar ) {
                $data['errors'] = $this->erroresCaptura;
            }
        }
        $data['menu'] = 'aspirante';
        $data['progreso'] = $this->getProgreso();
        if ( $guardar ) $data['guardado'] = true;
        return $this->selectView( 'perfil/salud', $data );
    }

    /**
     * genera la vista de los datos de emergencia del aspirante y guarda los datos cuando se recibe un post
     */
    public function emergencia() {
        $guardar = false;
        $guardado = false;
        if( !is_null( $this->request->getPost('gf') ) || !is_null( $this->request->getPost('ged') ) ) {
            $guardar = true;
            $guardado = $this->guardaEmergencia();
        }
        $data = $this->buscaEmergencia();
        if ( $guardado ) {
            if( !is_null($this->request->getPost('gf')) ) { // se esta finalizando la seccion
                $proceso = $this->procesosModel->find( $this->sesion->get('id') ); 
                $proceso->captura = '1'; 
                $this->procesosModel->save( $proceso ); 
                return redirect()->to( base_url('Documentos') );
            }
        }
        else {
            if ( $guardar ) {
                $data['errors'] = $this->erroresCaptura;
            }
        }
        $data['menu'] = 'aspirante';
        $data['progreso'] = $this->getProgreso();
        if ( $guardar ) $data['guardado'] = true;
        return $this->selectView( 'perfil/emergencia', $data );
    }

    /**
     * genera la vista de los datos de emergencia del aspirante y guarda los datos cuando se recibe un post
     */
    public function procedencia() {
        $localidadesModel = new LocalidadesModel();
        $guardar = false;
        $guardado = false;
        if( !is_null( $this->request->getPost('gf') ) || !is_null( $this->request->getPost('ged') ) ) {
            $guardar = true;
            $guardado = $this->guardaProcedencia();
        }
        $data = $this->buscaProcedencia();
        if ( $guardado ) {
            if( !is_null($this->request->getPost('gf')) ) { // se esta finalizando la seccion
                return redirect()->to( base_url('Perfil/Emergencia') );
            }
        }
        else {
            if ( $guardar ) {
                $data['errors'] = $this->erroresCaptura;
            }
        }
        $data['menu'] = 'aspirante';
        $data['progreso'] = $this->getProgreso();
        if ( $guardar ) $data['guardado'] = true;
        $data['estados'] = $localidadesModel->getEstadosCct();
        return $this->selectView( 'perfil/procedencia', $data );
    }

    /**
     * Genera el PDF para poder imprimir la credencial, redirigiendo a un archivo PDF
     * @throws PageNotFoundException tira una página de 404 al no encontrar la credencial del estudiante
     */
    public function fichaExamen() {
        return redirect()->to( base_url('Perfil') ); die();
        // Create an instance of the class:
        $mpdf = new \Mpdf\Mpdf( $this->configPdf() ); 
        $aspiranteModel = new AspirantesModel();
        $aspirante = $aspiranteModel->find( $this->sesion->get('id') );
        if ( !is_null($aspirante) ) {
            // Write some HTML code:
            $curl_handle=curl_init();
            curl_setopt($curl_handle, CURLOPT_URL, base_url('Perfil/ImpresionFicha').'/'.$aspirante->id );
            curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 2);
            curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
            $html = curl_exec($curl_handle); // var_dump( $html ); die();
            curl_close($curl_handle);
            $mpdf->WriteHTML($html);
            $content = $mpdf->Output( '', 'S'); 
            try {
                $filename = $aspirante->id.'_'.time().'.pdf';
                $file = fopen( env('ficha.dir').$filename, "w" );
                if ( $file ) {
                    fwrite( $file, $content );
                    fclose($file);
                    return redirect()->to( env('ficha.url').$filename );
                    unlink( env('ficha.dir').$filename );
                    die();
                }
            }
            catch(\Exception $ex) {
                throw $ex;
            }
        }
        throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
    }

    public function impresionFicha($id) {
        $aspiranteModel = new AspirantesModel();
        $aspirante = $aspiranteModel->find($id);
        $carrerasModel = new CarrerasModel();
        $documentosModel = new DocumentosModel();
        if ( $aspirante ) {
            $data['folio'] = $id;
            $data['nombre'] = $aspirante->apPaterno.' '.$aspirante->apMaterno.' '.$aspirante->nombre;
            $data['curp'] = $aspirante->curp;
            $data['fecha'] = $aspirante->examenFecha;
            $data['hora'] = $aspirante->examenHora;
            $carrera = $carrerasModel->find( $aspirante->carrera );
            $data['carrera'] = $carrera->nombre;
            $documentos = $documentosModel->getArchivos( $aspirante->id );
            foreach ( $documentos as $documento ) {
                if ( $documento->tipo == '9' ) {
                    try {
                        $file = fopen( $documento->ruta, 'r' );
                        if ( $file ) {
                            $datos = fread( $file, filesize($documento->ruta) );
                            $data['foto'] = 'data:image/jpg;base64,' .base64_encode( $datos );
                            fclose($file);
                        }
                    }
                    catch (\Exception $ex) {
                        echo $ex->getMessage();
                    }
                }
            }
            return view('perfil/fichaexamen', $data);
        }
        throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
    }

    // -----------------------------------
    /**
     * Genera el PDF para poder imprimir la credencial, redirigiendo a un archivo PDF
     * @throws PageNotFoundException tira una página de 404 al no encontrar la credencial del estudiante
     */
    public function cartaAceptacion() {
        // Create an instance of the class:
        $mpdf = new \Mpdf\Mpdf( $this->configPdf() ); 
        $aspiranteModel = new AspirantesModel();
        $aspirante = $aspiranteModel->find( $this->sesion->get('id') );
        if ( !is_null($aspirante) ) {
            // Write some HTML code:
            $curl_handle=curl_init();
            curl_setopt($curl_handle, CURLOPT_URL, base_url('Perfil/ImpresionCarta').'/'.$aspirante->id );
            curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 2);
            curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
            $html = curl_exec($curl_handle); // var_dump( $html ); die();
            curl_close($curl_handle);
            $mpdf->WriteHTML($html);
            $content = $mpdf->Output( '', 'S'); 
            try {
                $filename = $aspirante->id.'_'.time().'_ACEPTADO.pdf';
                $file = fopen( env('ficha.dir').$filename, "w" );
                if ( $file ) {
                    fwrite( $file, $content );
                    fclose($file);
                    return redirect()->to( env('ficha.url').$filename );
                    unlink( env('ficha.dir').$filename );
                    die();
                }
            }
            catch(\Exception $ex) {
                throw $ex;
            }
        }
        throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
    }

    public function impresionCarta($id) {
        $aspiranteModel = new AspirantesModel();
        $aspirante = $aspiranteModel->find($id);
        $carrerasModel = new CarrerasModel();
        $documentosModel = new DocumentosModel();
        if ( $aspirante ) {
            $data['id'] = $id;
            $data['nombre'] = $aspirante->nombre;
            $data['apPaterno'] = $aspirante->apPaterno;
            $data['apMaterno'] = $aspirante->apMaterno;
            $data['puntaje'] = $aspirante->califFinal;
            $data['aceptado'] = $aspirante->aceptado;
            $carrera = $carrerasModel->find( $aspirante->carrera );
            $data['carrera'] = $carrera->nombre;
            
            return view('perfil/cartaaceptacion', $data);
        }
        throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
    }





    // -----------------------------------

    /**
     * guarda los datos generales del aspirante
     * @return true si se puede guardar todo de forma correcta
     * @return false si ocurre un error
     */
    private function guardaGenerales() {
        $returnSave = true;
        $aspirantesModel = new AspirantesModel();
        $aspirante = $aspirantesModel->find( $this->sesion->get('id') );
        $datos = $this->request->getPost();
        if ( array_key_exists( 'gf', $datos) ) {
            $datos['enviado'] = '1';
            $validacion = $this->valida( $datos, 'generales' );
            if ( $validacion !== true ) {
                $datos['enviado'] = '0';
                $this->erroresCaptura = $validacion;
                $returnSave = false;
            }
        }
        if ( !is_null($aspirante) ) {
            try {
                $aspirante->fill( $datos );
                $aspirantesModel->save( $aspirante );
                return $returnSave;
            }
            catch ( \Exception $ex ) {
                $this->erroresCaptura[] = 'Ocurrió un error al guardar los datos. '.$ex->getMessage();        
            }
        }
        $this->erroresCaptura[] = 'No se pudo encontrar el aspirante.';
        return false;
    }

    /**
     * guarda los datos generales del aspirante
     * @return true si se puede guardar todo de forma correcta
     * @return false si ocurre un error
     */
    private function guardaSocioeconomicos() {
        $returnSave = true;
        $socioeconomicosModel = new SocioeconomicosModel();
        $socioeconomico = $socioeconomicosModel->find( $this->sesion->get('id') );
        $datos = $this->request->getPost();
        if ( array_key_exists( 'gf', $datos) ) {
            $datos['enviado'] = '1';
            $validacion = $this->valida( $datos, 'socioeconomico' );
            if ( $validacion !== true ) {
                $datos['enviado'] = '0';
                $this->erroresCaptura = $validacion;
                $returnSave = false;
            }
        }
        if ( !is_null($socioeconomico) ) {
            try {
                $socioeconomico->fill( $datos );
                $socioeconomicosModel->save( $socioeconomico );
                return $returnSave;
            }
            catch ( \Exception $ex ) {
                $this->erroresCaptura[] = 'Ocurrió un error al guardar los datos. '.$ex->getMessage();        
            }
        }
        $this->erroresCaptura[] = 'No se pudo encontrar el registro de los datos socioeconomicos.';
        return false;
    }

    /**
     * guarda los datos generales del aspirante
     * @return true si se puede guardar todo de forma correcta
     * @return false si ocurre un error
     */
    private function guardaSalud() {
        $returnSave = true;
        $saludModel = new SaludModel();
        $salud = $saludModel->find( $this->sesion->get('id') );
        $datos = $this->request->getPost();
        if ( array_key_exists( 'gf', $datos) ) {
            $datos['enviado'] = '1';
            $validacion = $this->valida( $datos, 'salud' );
            if ( $validacion !== true ) {
                $datos['enviado'] = '0';
                $this->erroresCaptura = $validacion;
                $returnSave = false;
            }
        }
        if ( !is_null($salud) ) {
            try {
                $salud->fill( $datos );
                $saludModel->save( $salud );
                return $returnSave;
            }
            catch ( \Exception $ex ) {
                $this->erroresCaptura[] = 'Ocurrió un error al guardar los datos. '.$ex->getMessage();        
            }
        }
        $this->erroresCaptura[] = 'No se pudo encontrar el registro de los datos de salud.';
        return false;
    }

    /**
     * guarda los datos generales del aspirante
     * @return true si se puede guardar todo de forma correcta
     * @return false si ocurre un error
     */
    private function guardaProcedencia() {
        $returnSave = true;
        $procedenciasModel = new ProcedenciasModel();
        $procedencia = $procedenciasModel->find( $this->sesion->get('id') );
        $datos = $this->request->getPost();
        if ( array_key_exists( 'gf', $datos) ) {
            $datos['enviado'] = '1';
            $validacion = $this->valida( $datos, 'procedencia' );
            if ( $validacion !== true ) {
                $datos['enviado'] = '0';
                $this->erroresCaptura = $validacion;
                $returnSave = false;
            }
        }
        if ( !is_null($procedencia) ) {
            try {
                $procedencia->fill( $datos ); 
                $procedenciasModel->save( $procedencia );
                return $returnSave;
            }
            catch ( \Exception $ex ) {
                $this->erroresCaptura[] = 'Ocurrió un error al guardar los datos. '.$ex->getMessage();        
            }
        }
        $this->erroresCaptura[] = 'No se pudo encontrar el registro de los datos de salud.';
        return false;
    }

    /**
     * guarda los datos generales del aspirante
     * @return true si se puede guardar todo de forma correcta
     * @return false si ocurre un error
     */
    private function guardaEmergencia() {
        $returnSave = true;
        $contactosEmergenciaModel = new ContactosEmergenciaModel();
        $emergencia = $contactosEmergenciaModel->find( $this->sesion->get('id') );
        $datos = $this->request->getPost();
        if ( array_key_exists( 'gf', $datos) ) {
            $datos['enviado'] = '1';
            $validacion = $this->valida( $datos, 'emergencia' );
            if ( $validacion !== true ) {
                $datos['enviado'] = '0';
                $this->erroresCaptura = $validacion;
                $returnSave = false;
            }
        }
        if ( !is_null($emergencia) ) {
            try {
                $emergencia->fill( $datos ); 
                $contactosEmergenciaModel->save( $emergencia );
                return $returnSave;
            }
            catch ( \Exception $ex ) {
                $this->erroresCaptura[] = 'Ocurrió un error al guardar los datos. '.$ex->getMessage();        
            }
        }
        $this->erroresCaptura[] = 'No se pudo encontrar el registro de los datos de salud.';
        return false;
    }


    /**
     * busca los datos generales del aspirante con sesion iniciada
     * y devuelte un arreglo para ser utilizado por la vista
     * @return datos el arreglo con los datos encontrados, o con los campos vacios si no se encuentra
     */
    private function buscaGenerales() {
        $datos = [
            'id' => '', 'carrera' => '', 'apPaterno' => '', 'apMaterno' => '', 'nombre' => '', 'curp' => '', 'correo' => '',
            'telefono' => '', 'calle' => '', 'numero' => '', 'numeroInterior' => '', 'cp' => '', 'colonia' => '', 'municipio' => '',
            'estado' => '', 'pais' => '', 'nacimientoFecha' => '', 'nacimientoMunicipio' => '', 'nacimientoEstado' => '', 'nacimientoPais' => '', 'enviado'=>'0'
        ];
        $aspirantesModel = new AspirantesModel();
        $aspirante = $aspirantesModel->find( $this->sesion->get('id') );
        if ( !is_null($aspirante) ) {
            $datos = [
                'id'                    => $aspirante->id,
                'carrera'               => $aspirante->carrera,
                'apPaterno'             => $aspirante->apPaterno,
                'apMaterno'             => $aspirante->apMaterno,
                'nombre'                => $aspirante->nombre,
                'curp'                  => $aspirante->curp,
                'correo'                => $aspirante->correo,
                'telefono'              => $aspirante->telefono,
                'calle'                 => ( ( is_null($aspirante->calle) )? '': $aspirante->calle ),
                'numero'                => ( ( is_null($aspirante->numero) )? '': $aspirante->numero ),
                'numeroInterior'        => ( ( is_null($aspirante->numeroInterior) )? '': $aspirante->numeroInterior ),
                'cp'                    => ( ( is_null($aspirante->cp) )? '': $aspirante->cp ),
                'colonia'               => ( ( is_null($aspirante->colonia) )? '': $aspirante->colonia ),
                'municipio'             => ( ( is_null($aspirante->municipio) )? '': $aspirante->municipio ),
                'estado'                => ( ( is_null($aspirante->estado) )? '': $aspirante->estado ),
                'pais'                  => ( ( is_null($aspirante->pais) )? '': $aspirante->pais ),
                'nacimientoFecha'       => ( ( is_null($aspirante->nacimientoFecha) )? '': $aspirante->nacimientoFecha ),
                'nacimientoMunicipio'   => ( ( is_null($aspirante->nacimientoMunicipio) )? '': $aspirante->nacimientoMunicipio ),
                'nacimientoEstado'      => ( ( is_null($aspirante->nacimientoEstado) )? '': $aspirante->nacimientoEstado ),
                'nacimientoPais'        => ( ( is_null($aspirante->nacimientoPais) )? '': $aspirante->nacimientoPais ),
                'nacimientoPais'        => ( ( is_null($aspirante->nacimientoPais) )? '': $aspirante->nacimientoPais ),
                'enviado'               => ( ( is_null($aspirante->enviado) )? '0': $aspirante->enviado ),
            ];
        }
        return $datos;
    }  

    /**
     * busca los datos generales del aspirante con sesion iniciada
     * y devuelte un arreglo para ser utilizado por la vista
     * @return datos el arreglo con los datos encontrados, o con los campos vacios si no se encuentra
     */
    private function buscaSocioeconomicos() {
        $datos = [
            'aspirante' => '', 'estadoCivil' => '', 'hijos' => '', 'mpSoltero' => '', 'personasCasa' => '', 'viveEn' => '', 'trabajaSalud' => '',
            'comunidadIndigena' => '', 'situacionVulnerable' => '', 'tieneDesktop' => '', 'tieneWebcam' => '', 'tieneLaptop' => '', 'tieneInternet' => '', 'tieneTablet' => '',
            'beca' => '', 'enviado' => '', 'dependientes' => '', 'radicar' => '', 'vulnerableCual' => '',
            'admisionPrevio' => '', 'admisionActual' => '', 'tieneSmartphone' => '', 'examenOnline' => '',
        ];
        $aspirantesModel = new SocioeconomicosModel();
        $socioeconomico = $aspirantesModel->find( $this->sesion->get('id') );
        if ( !is_null($socioeconomico) ) {
            $datos = [
                'aspirante'             => $socioeconomico->id,
                'estadoCivil'           => ( ( is_null($socioeconomico->estadoCivil) )? '': $socioeconomico->estadoCivil ),
                'hijos'                 => ( ( is_null($socioeconomico->hijos) )? '': $socioeconomico->hijos ),
                'mpSoltero'             => ( ( is_null($socioeconomico->mpSoltero) )? '': $socioeconomico->mpSoltero ),
                'personasCasa'          => ( ( is_null($socioeconomico->personasCasa) )? '': $socioeconomico->personasCasa ),
                'viveEn'                => ( ( is_null($socioeconomico->viveEn) )? '': $socioeconomico->viveEn ),
                'trabajaSalud'          => ( ( is_null($socioeconomico->trabajaSalud) )? '': $socioeconomico->trabajaSalud ),
                'comunidadIndigena'     => ( ( is_null($socioeconomico->comunidadIndigena) )? '': $socioeconomico->comunidadIndigena ),
                'situacionVulnerable'   => ( ( is_null($socioeconomico->situacionVulnerable) )? '': $socioeconomico->situacionVulnerable ),
                'tieneDesktop'          => ( ( is_null($socioeconomico->tieneDesktop) )? '': $socioeconomico->tieneDesktop ),
                'tieneWebcam'           => ( ( is_null($socioeconomico->tieneWebcam) )? '': $socioeconomico->tieneWebcam ),
                'tieneLaptop'           => ( ( is_null($socioeconomico->tieneLaptop) )? '': $socioeconomico->tieneLaptop ),
                'tieneInternet'         => ( ( is_null($socioeconomico->tieneInternet) )? '': $socioeconomico->tieneInternet ),
                'tieneTablet'           => ( ( is_null($socioeconomico->tieneTablet) )? '': $socioeconomico->tieneTablet ),
                'beca'                  => ( ( is_null($socioeconomico->beca) )? '': $socioeconomico->beca ),
                'enviado'               => ( ( is_null($socioeconomico->enviado) )? '': $socioeconomico->enviado ),

                'dependientes'          => ( ( is_null($socioeconomico->dependientes) )? '': $socioeconomico->dependientes ),
                'radicar'               => ( ( is_null($socioeconomico->radicar) )? '': $socioeconomico->radicar ),
                'vulnerableCual'        => ( ( is_null($socioeconomico->vulnerableCual) )? '': $socioeconomico->vulnerableCual ),
                'admisionPrevio'        => ( ( is_null($socioeconomico->admisionPrevio) )? '': $socioeconomico->admisionPrevio ),
                'admisionActual'        => ( ( is_null($socioeconomico->admisionActual) )? '': $socioeconomico->admisionActual ),
                'tieneSmartphone'       => ( ( is_null($socioeconomico->tieneSmartphone) )? '': $socioeconomico->tieneSmartphone ),
                'examenOnline'          => ( ( is_null($socioeconomico->examenOnline) )? '': $socioeconomico->examenOnline ),
                'ingreso'               => ( ( is_null($socioeconomico->ingreso) )? '': $socioeconomico->ingreso ),
                'conseguirCompu'        => ( ( is_null($socioeconomico->conseguirCompu) )? '': $socioeconomico->conseguirCompu ),
            ];
        }
        return $datos;
    }

    /**
     * busca los datos generales del aspirante con sesion iniciada
     * y devuelte un arreglo para ser utilizado por la vista
     * @return datos el arreglo con los datos encontrados, o con los campos vacios si no se encuentra
     */
    private function buscaSalud() {
        $datos = [
            'aspirante' => '', 'tipoSangre' => '', 'padecimientos' => '', 'discapacidad' => '', 'certificadoDiscapacidad' => '', 'usaLentes' => '', 'tieneProtesis' => '',
            'servicioMedico' => '', 'enviado' => '', 
        ];
        $saludModel = new SaludModel();
        $salud = $saludModel->find( $this->sesion->get('id') );
        if ( !is_null($salud) ) {
            $datos = [
                'aspirante'                 => $salud->aspirante,
                'tipoSangre'                => ( ( is_null($salud->tipoSangre) )? '': $salud->tipoSangre ),
                'padecimientos'             => ( ( is_null($salud->padecimientos) )? '': $salud->padecimientos ),
                'discapacidad'              => ( ( is_null($salud->discapacidad) )? '': $salud->discapacidad ),
                'certificadoDiscapacidad'   => ( ( is_null($salud->certificadoDiscapacidad) )? '': $salud->certificadoDiscapacidad ),
                'usaLentes'                 => ( ( is_null($salud->usaLentes) )? '': $salud->usaLentes ),
                'tieneProtesis'             => ( ( is_null($salud->tieneProtesis) )? '': $salud->tieneProtesis ),
                'servicioMedico'            => ( ( is_null($salud->servicioMedico) )? '': $salud->servicioMedico ),
                'enviado'                   => ( ( is_null($salud->enviado) )? '': $salud->enviado ),
            ];
        }
        return $datos;
    }

    /**
     * busca los datos generales del aspirante con sesion iniciada
     * y devuelte un arreglo para ser utilizado por la vista
     * @return datos el arreglo con los datos encontrados, o con los campos vacios si no se encuentra
     */
    private function buscaProcedencia() {
        $datos = [
            'aspirante' => '', 'tipo' => '', 'cct' => '', 'nombre' => '', 'calle' => '', 'num' => '', 'cp' => '',
            'colonia' => '', 'municipio' => '', 'estado' => '', 'pais' => '', 'fechaConclusion' => '', 
            'promedio' => '', 'enviado' => '',
        ];
        $procedenciasModel = new ProcedenciasModel();
        $procedencia = $procedenciasModel->find( $this->sesion->get('id') );
        if ( !is_null($procedencia) ) {
            $datos = [
                'aspirante'         => $procedencia->aspirante,
                'tipo'              => ( ( is_null($procedencia->tipo) )? '': $procedencia->tipo ),
                'cct'               => ( ( is_null($procedencia->cct) )? '': $procedencia->cct ),
                'nombre'            => ( ( is_null($procedencia->nombre) )? '': $procedencia->nombre ),
                'calle'             => ( ( is_null($procedencia->calle) )? '': $procedencia->calle ),
                'num'               => ( ( is_null($procedencia->num) )? '': $procedencia->num ),
                'cp'                => ( ( is_null($procedencia->cp) )? '': $procedencia->cp ),
                'colonia'           => ( ( is_null($procedencia->colonia) )? '': $procedencia->colonia ),
                'municipio'         => ( ( is_null($procedencia->municipio) )? '': $procedencia->municipio ),
                'estado'            => ( ( is_null($procedencia->estado) )? '': $procedencia->estado ),
                'pais'              => ( ( is_null($procedencia->pais) )? '': $procedencia->pais ),
                'fechaConclusion'   => ( ( is_null($procedencia->fechaConclusion) )? '': $procedencia->fechaConclusion ),
                'promedio'          => ( ( is_null($procedencia->promedio) )? '': $procedencia->promedio ),
                'enviado'           => ( ( is_null($procedencia->enviado) )? '': $procedencia->enviado ),
            ];
        }
        return $datos;
    }

    /**
     * busca los datos generales del aspirante con sesion iniciada
     * y devuelte un arreglo para ser utilizado por la vista
     * @return datos el arreglo con los datos encontrados, o con los campos vacios si no se encuentra
     */
    private function buscaEmergencia() {
        $datos = [
            'aspirante' => '', 'parentesco' => '', 'telefono' => '', 'correo' => '', 'enviado' => '', 
        ];
        $contactosEmergenciaModel = new ContactosEmergenciaModel();
        $contacto = $contactosEmergenciaModel->find( $this->sesion->get('id') );
        if ( !is_null($contacto) ) {
            $datos = [
                'aspirante'     => $contacto->aspirante,
                'nombre'        => ( ( is_null($contacto->nombre) )? '': $contacto->nombre ),
                'parentesco'    => ( ( is_null($contacto->parentesco) )? '': $contacto->parentesco ),
                'telefono'      => ( ( is_null($contacto->telefono) )? '': $contacto->telefono ),
                'correo'        => ( ( is_null($contacto->correo) )? '': $contacto->correo ),
                'enviado'       => ( ( is_null($contacto->enviado) )? '': $contacto->enviado ),
            ];
        }
        return $datos;
    }

    /**
     * devuelve el arreglo de configuracion de la pagina PDF para ficha de examen
     * @return array con los datos de configuracion
     */
    private function configPdf() {
        return [
            'mode' => 'utf-8', 
            'collapseBlockMargins'=>true, 
            'tempDir' => WRITEPATH,
            'format' => 'Letter',
            'margin_left' => 0,
            'margin_right' => 0,
            'margin_top' => 0,
            'margin_bottom' => 0,
            'margin_header' => 0,
            'margin_footer'   => 0
        ];
    }

}