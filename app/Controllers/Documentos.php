<?php
namespace App\Controllers;

use App\Models\TiposDocumentosModel;
use App\Models\DocumentosModel;
use App\Models\ProcesosModel;
use App\Models\AspirantesModel;

use App\Entities\Documento;

class Documentos extends BaseController {

    private $tiposDocumentosModel;
    private $documentosModel;
    private $procesosModel;

    private $errors = [];

    public function __construct() {
        $this->tiposDocumentosModel = new TiposDocumentosModel();
        $this->documentosModel = new DocumentosModel();
        $this->procesosModel = new ProcesosModel();
    }

    /**
     * muestra la pagina de carga de documentos y realiza el upload cuando se sube alguno
     * 
     */
    public function index() {
        $tipo = $this->request->getPost( 'tipo' );
        if ( !is_null($tipo) ) { // indica que se está subiendo un archivo
            $datosTipo = $this->getDataTipo( $tipo );
            if ( !is_null($datosTipo) ) {
                try {
                    $this->guardaArchivo($datosTipo);
                }
                catch(\Exception $ex) {

                }
            }
        }
        $data['tiposDocumentos'] = $this->tiposDocumentosModel->getTiposVigentes();
        $data['documentos'] = $this->getArchivosUsuario();
        $data['menu'] = 'aspirante';
        if ( count($this->errors) ) {
            $data['errors'] = $this->errors;
        }
        if ( count($this->errors)==0 && !is_null($tipo) ) {
            $data['success'] = true;
        }
        $data['progreso'] = $this->getProgreso();
        return view( 'documentos/carga', $data );
    }

    /**
     * realiza la finalización de la carga de documentos
     */
    public function finalizar() {
        $data['errors'] = [ 'No fue posible actualizar tus datos. Verifica que hayas iniciado sesión y que tus datos sean correctos.' ];
        $data['nombre'] = 'Aspirante';
        $aspirantesModel = new AspirantesModel();
        $aspirante = $aspirantesModel->find( $this->sesion->get('id') );
        if ( !is_null($aspirante) ) {
            try {
                $data['nombre'] = $aspirante->nombre;
                $proceso = $this->procesosModel->find( $aspirante->id );
                $proceso->documentos = '1';
                $proceso->revision = '0';
                $proceso->exito = '0';
                $proceso->declinado = '0';
                $this->procesosModel->save( $proceso );
                unset( $data['errors'] );
            }
            catch ( \Exception $ex ) {
                $data['errors'][] = 'Ocurrió un error en la actualización: '.$ex->getMessage().' Contáctanos en admision@usep.mx.';
            }
        }
        $data['progreso'] = $this->getProgreso();
        $data['menu'] = 'aspirante';
        return view( 'documentos/finaliza', $data );
    }

    public function ver($fileid) {
        $documento = $this->documentosModel->find( $fileid );
        if ( !is_null($documento) ) {
            if ( $documento->aspirante == $this->sesion->get('id') ) {
                if ( $file = fopen( $documento->ruta, 'r' ) ) {
                    if (file_exists($documento->ruta)) {
                        $outputname = $this->sesion->get('id').'-'.time();
                        $partes_ruta = pathinfo($documento->ruta);
                        header('Content-Description: File Transfer');
                        header('Content-Type: application/octet-stream');
                        header('Content-Disposition: attachment; filename="'.$outputname.'.'.$partes_ruta['extension'].'"');
                        header('Expires: 0');
                        header('Cache-Control: must-revalidate');
                        header('Pragma: public');
                        header('Content-Length: ' . filesize($documento->ruta));
                        readfile($documento->ruta);
                        exit;
                    }
                }
            }
        }
        throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
    }

    /**
     * obtiene los datos necesarios para validar el tipo
     * @param tipo el ID del tipo de archivo
     * @return datos los datos de tipo de archivo
     * @return null si no se encuentra el tipo de archivo
     */
    private function getDataTipo($tipo) {
        $tipoDocumento = $this->tiposDocumentosModel->find( $tipo );
        if ( !is_null($tipoDocumento) ) {
            $datos['tipo'] = $tipoDocumento->id;
            $datos['nombre'] = $tipoDocumento->nombre;
            $datos['extensiones'] = $tipoDocumento->formatos;
            return $datos;
        }
        return null;
    }

    private function guardaArchivo($datosTipo) {
        $validation =  \Config\Services::validation();
        $files = $this->request->getFiles();
        $fileRules = [ 'archivo' => 'uploaded[archivo]|ext_in[archivo,'.$datosTipo['extensiones'].']|max_size[archivo,2048]' ];
        $fileRules_errors = [ 'archivo' => [ 'uploaded' => 'No se detectó que subieras tu '.$datosTipo['nombre'].'.', 'ext_in'=>'Por favor sube un archivo con alguna de estas extensiones: '.$datosTipo['extensiones'].'.', 'max_size'=>'El tamaño de archivo no debe exceder los 2MB' ] ];
        $esValido = $this->validate( $fileRules , $fileRules_errors );
        if ( $esValido ) {
            $file = $files['archivo'];
            $ext = $file->getClientExtension();
            $newName = $this->sesion->get('id').'-'.$datosTipo['tipo'].'.'.$ext;
            if ( file_exists( WRITEPATH.'admision2021/'.$newName ) ) unlink(WRITEPATH.'admision2021/'.$newName);
            $file->move(WRITEPATH.'admision2021', $newName);
            if ( $file->hasMoved() ) {
                $documento = $this->documentosModel->busca( $this->sesion->get('id'), $datosTipo['tipo'] );
                if ( is_null($documento) ) {
                    $documento = new Documento();
                    $documento->aspirante = $this->sesion->get('id');
                    $documento->tipo = $datosTipo['tipo'];
                    $documento->ruta = WRITEPATH.'admision2021/'.$newName;
                    $documento->status = 'SUBIDO';
                    $this->documentosModel->insert( $documento );
                }
                else {
                    $documento->ruta = WRITEPATH.'admision2021/'.$file->getName();
                    $this->documentosModel->save( $documento );
                }
            }
            else {
                $this->errors[] = 'No se pudo escribir el archivo.';
            }
        }
        else {
            $errores = $validation->getErrors();
            $this->errors[] = $errores['archivo'];
        }
    }

    /**
     * obtiene un arreglo con los archivos 
     */
    private function getArchivosUsuario() {
        $documentos = array();
        $archivos = $this->documentosModel->getArchivos( $this->sesion->get('id') );
        foreach ( $archivos as $archivo ) {
            $documentos[ $archivo->tipo ] = $archivo;
        }
        return $documentos;
    }

}