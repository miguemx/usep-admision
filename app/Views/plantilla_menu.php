<?php echo $this->extend('plantilla'); ?>

<?php echo $this->section('content') ?>


    <?php
        if ( isset($menu) ) {
            echo $this->include('menus/'.$menu);
        } 
        else {
            echo $this->include('menus/default');
        }
    ?>
    
    

    <?php $this->renderSection('workarea'); ?>


<?php echo $this->endSection() ?>
