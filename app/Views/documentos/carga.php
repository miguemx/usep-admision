<?php echo $this->extend('plantilla_menu'); ?>

<?php echo $this->section('workarea') ?>

<?php 
$i = 0;
$total = count( $tiposDocumentos );
?>


<div>&nbsp;</div>
<?php echo $this->include('perfil/progreso'); ?>
<div>&nbsp;</div>

<div class="container-fluid">
    <div class="row">
        
        <?php echo $this->include('menus/lateralaspirantes'); ?>
        
        <div class="col-sm-9">
            <div class="card">
                <div class="card-header">
                    <strong>Carga de documentos</strong>
                </div>
                <div class="card-body">
                    <?php if ( isset($errors) ): ?>
                        <div class="alert alert-danger">
                            <?php foreach( $errors as $error ): ?>
                            <?php echo $error; ?><br />
                            <?php endforeach; ?>
                        </div>
                    <?php endif; ?>
                    <?php if ( isset($success) ): ?>
                        <div class="alert alert-success">
                            Tu archivo se ha subido correctamente.
                        </div>
                    <?php endif; ?>
                    <div>Entiendo que al subir los archivos que son solicitados, <strong>acepto</strong> que mis datos son verdaderos y no podré modificarlos posteriormente</div>
                    <?php foreach( $tiposDocumentos as $tipoDocumento ): ?>
                        <div class="alert alert-secondary">
                            <form action="<?php echo base_url('Documentos'); ?>" method="post" enctype="multipart/form-data">
                                <label for="file-<?php echo $tipoDocumento->nombre ?>" class="form-label"><?php echo $tipoDocumento->nombre ?></label>
                                <div class="row">
                                    <div class="col-sm-4">
                                        <input class="form-control form-control-sm" id="file-<?php echo $tipoDocumento->nombre ?>" name="archivo" type="file" />
                                        <input type="hidden" name="tipo" value="<?php echo $tipoDocumento->id; ?>" />
                                    </div>
                                    <div class="col-sm-2">
                                        <?php if($progreso['global']['documentos'] == '0'): ?>
                                            <button type="submit" class="btn btn-secondary btn-sm">Enviar</button>
                                        <?php endif; ?>
                                        
                                        <?php if( isset($documentos[$tipoDocumento->id]) ): $i++; ?>
                                            <a href="<?php echo base_url('Documentos/Ver').'/'.$documentos[ $tipoDocumento->id ]->id; ?>" target="_blank" class="btn btn-secondary btn-sm">Ver</a>
                                        <?php endif; ?>
                                    </div>
                                    <div class="col-sm-6">
                                        <?php if( isset($documentos[$tipoDocumento->id]) ): ?>
                                            <?php echo $documentos[ $tipoDocumento->id ]->comentarios; ?>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </form>
                        </div>
                    <?php endforeach; ?>
                    <?php if( $i >= $total && $progreso['global']['documentos'] == 'yanosepuedacargar;aquidbeiruncero' ): ?>
                        <div>
                            <form method="post" action="<?php echo base_url('Documentos/Finalizar') ?>">
                                <button class="btn btn-secondary" type="submit">
                                    Finalizar la carga de documentos.
                                </button>
                            </form>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>


<?php echo $this->endSection() ?>