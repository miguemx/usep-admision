<?php echo $this->extend('plantilla_menu'); ?>

<?php echo $this->section('workarea') ?>



<div>&nbsp;</div>
<?php echo $this->include('perfil/progreso'); ?>
<div>&nbsp;</div>

<div class="container-fluid">
    <div class="row">
        
        <?php echo $this->include('menus/lateralaspirantes'); ?>
        
        <div class="col-sm-9">
            <div class="card">
                <div class="card-header">
                    <strong>Documentos</strong>
                </div>
                <div class="card-body">
                    <?php if ( isset($errors) ): ?>
                        <div class="alert alert-danger">
                            <?php foreach( $errors as $error ): ?>
                            <?php echo $error; ?><br />
                            <?php endforeach; ?>
                        </div>
                    <?php endif; ?>

                    <div>
                        <p><strong>&iexcl;<?php echo $nombre; ?>!</strong></p>
                        <p>
                            Has concluido exitosamente tu registro y carga de documentos, mismos que pasarán a una etapa de validación. 
                            Podrás verificar su avance u observaciones en el siguiente enlace:<br />
                            <a href="<?php echo base_url('Perfil') ?>">Portal de aspirantes</a>
                        </p>
                        <p>
                            Te sugerimos mantenerte atento(a) a la actualización de tu estatus, ya que de ser validados exitosamente, 
                            se habilitará la opción para continuar con la <strong>Fase 3</strong> de la convocatoria de admisión 2021.
                        </p>
                        <p>
                            El periodo para la impresión de la <strong>&quot;Ficha de examen&quot;</strong> será del 17 al 21 de junio del 2021.
                        </p>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</div>


<?php echo $this->endSection() ?>