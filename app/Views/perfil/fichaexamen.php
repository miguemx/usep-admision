<body style="margin: 0px; padding: 0px; font-family: Arial, Helvetica, sans-serif;">
    <div style="font-weight: bold; text-align: center; background-image: url('<?php echo base_url('ficha/header.png') ?>'); background-repeat: no-repeat; background-size: 70%; padding-top: 150px;">
        FICHA DE INGRESO AL EXAMEN DE ADMISIÓN
    </div>
    <div style="background-image: url('<?php echo base_url('ficha/fondozaca.png') ?>'); background-repeat: no-repeat; background-position: left bottom; background-size: 60%;">
        <div style="margin: 20px 50px; border: 1px solid #000;">
            <table style="width: 100%;">
                <tr>
                    <td width="25%" rowspan="11" style="text-align: center;">
                        <?php if( isset($foto) ): ?>
                            <img src="<?php echo $foto; ?>" style="max-width: 120px; max-heigth: 150px;" />
                        <?php endif; ?>
                    </td>
                </tr>
                <tr>
                    <td style="margin-bottom: 10px;">
                        <strong>Folio:</strong> <?php echo $folio; ?>
                    </td>
                </tr>
                <tr>
                    <td style="margin-bottom: 10px;"> 
                        <strong>Nombre:</strong> <?php echo $nombre; ?>
                    </td>
                </tr>
                <tr>
                    <td style="margin-bottom: 10px;">
                        <strong>CURP:</strong> <?php echo $curp; ?>
                    </td>
                </tr>
                <tr>
                    <td style="margin-bottom: 10px">
                        <strong>Opción de ingreso:</strong> <?php echo $carrera; ?>
                    </td>
                </tr>
                <tr>
                    <td style="margin-bottom: 10px;">
                        <strong>Modalidad de examen:</strong> EN LÍNEA
                    </td>
                </tr>
                <tr>
                    <td style="margin-bottom: 10px;">
                        <strong>Día:</strong> <?php echo $fecha; ?>
                    </td>
                </tr>
                <tr>
                    <td style="margin-bottom: 10px;">
                        <strong>Hora:</strong> <?php echo $hora; ?>
                    </td>
                </tr>
                <tr>
                    <td style="margin-bottom: 10px;">
                        <strong>Usuario:</strong> <?php echo $folio; ?>
                    </td>
                </tr>
                <tr>
                    <td style="margin-bottom: 10px;">
                        <strong>Contraseña:</strong> <?php echo substr( $curp, 0, 4 ); ?>
                    </td>
                </tr>
                <tr>
                    <td style="margin-bottom: 10px;">
                        <strong>Liga de acceso:</strong> https://xbingreso.com/entrar/USEP<br />
                        
                        
                    </td>
                </tr>
            </table>
        </div>
        <div style="margin: 20px 50px; ">
            <p style="font-weight: bold;">
                Instrucciones generales:
            </p>
            <ol>
                <li>
                    <strong>Verifica el día y hora asignado para presentar tu examen de admisión.</strong><br />
                    <span style="font-weight: normal;">Importante: no habrá cambio de fecha u hora.</span>
                </li>
                <li>
                    <strong>Asegúrate que tu equipo de cómputo</strong>(cámara, micrófono, teclado y mouse) así como tu
                    conexión a internet, <strong>se encuentren en óptimas condiciones</strong>.
                    <ul>
                        <li>
                            Sistema operativo Windows Vista (R) o superior
                        </li>
                        <li>
                            Navegador Google Chrome <strong>versión más actual (90 o superior)</strong>
                        </li>
                        <li>
                            <em>La velocidad de conexión de tu internet al menos deberá contar con 5 Mbps de descarga
                            y 2 Mbps de subida</em>
                        </li>
                    </ul>
                </li>
                <li>
                    <strong>El día del examen ten a la mano una identificación oficial y la presente &quot;Ficha de Examen&quot;</strong>
                </li>
                <li>
                    <strong>Conéctate puntualmente</strong> en el día asignado para la aplicación de tu examen.
                </li>
                <li>
                    Recuerda que, en tu espacio de trabajo, <strong>únicamente podrás tener a la mano hojas blancas, lápiz,
                    goma y sacapuntas</strong> en caso de ser necesario. <strong>Está prohibido el uso de cualquier auxiliar, como lo
                    son calculadoras, celulares y tabletas</strong>.
                </li>
                <li>
                Una vez verificados los puntos anteriores y <strong>solo en caso de solicitar “apoyo técnico”</strong>, podrás ponerte en contacto mediante WhatsApp al: 5511909011. 
No se atenderá otro tipo de dudas que no sean situaciones técnicas para la aplicación de tu examen.
                </li>
                <li>
                    Manual de plataforma: https://exbach-ingreso.com/pdf/ManualZoomExbachv1.pdf<br />
                    Reglamento para aplicacion del examen: https://exbach-ingreso.com/pdf/Reglamento%20BPH_DAJ%20vf.pdf
                </li>
            </ol>
        </div>
        <div style="margin: 20px 50px; ">
            <p style="font-weight: bold;">
                La ficha es personal e intransferible, el uso indebido de la misma, será causa de la cancelación del examen,
                así como del Proceso de Admisión
            </p>
        </div>
    </div>
</body>
