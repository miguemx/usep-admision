<?php echo $this->extend('plantilla_menu'); ?>

<?php echo $this->section('workarea') ?>


<div>&nbsp;</div>
<?php echo $this->include('perfil/progreso'); ?>
<div>&nbsp;</div>

<div class="container-fluid">
    <div class="row">
        
        <?php echo $this->include('menus/lateralaspirantes'); ?>
        
        <div class="col-sm-9">
            <div class="card">
                <div class="card-header">
                    <strong>Datos Socioeconómicos</strong>
                </div>
                <div class="card-body">
                    <?php if(isset($guardado) ): ?>
                        <div class="alert alert-success">
                            Tu información ha sido guardada.
                        </div>
                    <?php endif; ?>
                    <?php if(isset($errors) ): ?>
                        <div class="alert alert-danger">
                            Ocurrieron los siguientes errores:<br />
                            <?php foreach($errors as $error): ?>
                                <?php echo $error; ?><br />
                            <?php endforeach; ?>
                        </div>
                    <?php endif; ?>
                    <form action="<?php echo base_url('Perfil/Socioeconomicos') ?>" method="post">
                        <div class="col-sm-12">
                            <label for="apPaterno" class="form-label">Escriba a cuanto asciende su ingreso familiar mensual en pesos mexicanos.</label>
                            <input type="text" class="form-control" id="txtapPaterno" name="ingreso" value="<?php echo $ingreso ?>"  />
                        </div>
                        
                        <div class="col-sm-12">
                            <label for="apPaterno" class="form-label">Estado civil: </label>
                            <select class="form-control" name="estadoCivil">
                                <option value="SOLTERO" <?php if($estadoCivil=='SOLTERO') echo 'selected="selected"';  ?>>SOLTERO</option>
                                <option value="CASADO" <?php if($estadoCivil=='CASADO') echo 'selected="selected"';  ?>>CASADO</option>
                                <option value="UNION LIBRE" <?php if($estadoCivil=='UNION LIBRE') echo 'selected="selected"';  ?>>UNION LIBRE</option>
                            </select>
                        </div>
                        <div>&nbsp;</div>
                        <div class="col-sm-12">
                            <label for="apPaterno" class="form-label">Núm. de hijos</label>
                            <select name="hijos" class="form-control">
                                <option value="0" <?php if($hijos=='0') echo 'selected="selected"';  ?>>No tengo hijos</option>
                                <option value="1" <?php if($hijos=='1') echo 'selected="selected"';  ?>>1</option><option value="2" <?php if($hijos=='2') echo 'selected="selected"';  ?>>2</option>
                                <option value="3" <?php if($hijos=='3') echo 'selected="selected"';  ?>>3</option><option value="4" <?php if($hijos=='4') echo 'selected="selected"';  ?>>4</option>
                                <option value="5" <?php if($hijos=='5') echo 'selected="selected"';  ?>>5</option><option value="6" <?php if($hijos=='6') echo 'selected="selected"';  ?>>6</option>
                                <option value="7" <?php if($hijos=='7') echo 'selected="selected"';  ?>>7</option><option value="8" <?php if($hijos=='8') echo 'selected="selected"';  ?>>8</option>
                                <option value="9" <?php if($hijos=='9') echo 'selected="selected"';  ?>>9</option><option value="10" <?php if($hijos=='10') echo 'selected="selected"';  ?>>10 o más</option>
                            </select>
                        </div>
                        <div>&nbsp;</div>
                        <div class="col-sm-12">
                            <label for="apPaterno" class="form-label">Núm. de dependientes económicos</label>
                            <select name="dependientes" class="form-control">
                                <option value="0" <?php if($dependientes=='0') echo 'selected="selected"';  ?>>Ninguno</option>
                                <option value="1" <?php if($dependientes=='1') echo 'selected="selected"';  ?>>1</option><option value="2" <?php if($dependientes=='2') echo 'selected="selected"';  ?>>2</option>
                                <option value="3" <?php if($dependientes=='3') echo 'selected="selected"';  ?>>3</option><option value="4" <?php if($dependientes=='4') echo 'selected="selected"';  ?>>4</option>
                                <option value="5" <?php if($dependientes=='5') echo 'selected="selected"';  ?>>5</option><option value="6" <?php if($dependientes=='6') echo 'selected="selected"';  ?>>6</option>
                                <option value="7" <?php if($dependientes=='7') echo 'selected="selected"';  ?>>7</option><option value="8" <?php if($dependientes=='8') echo 'selected="selected"';  ?>>8</option>
                                <option value="9" <?php if($dependientes=='9') echo 'selected="selected"';  ?>>9</option><option value="10" <?php if($dependientes=='10') echo 'selected="selected"';  ?>>10 o más</option>
                            </select>
                        </div>
                        <div>&nbsp;</div>
                        <div class="col-sm-12">
                            <label for="apPaterno" class="form-label">&iquest;Es madre o padre soltero?: </label>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" id="mpSoltero1" name="mpSoltero" value="1" <?php if($mpSoltero=='1') echo 'checked="checked"';  ?>>
                                <label class="form-check-label" for="casado1">Sí</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" id="mpSoltero2" name="mpSoltero" value="0" <?php if($mpSoltero!='1') echo 'checked="checked"';  ?>>
                                <label class="form-check-label" for="casado2">No</label>
                            </div>
                        </div>
                        <div>&nbsp;</div>
                        <div class="col-sm-12">
                            <label for="personasCasa" class="form-label">Núm. de personas que viven en su casa</label>
                            <select name="personasCasa" class="form-control">
                                <option value="1" <?php if($personasCasa=='1') echo 'selected="selected"';  ?>>Solamente yo</option><option value="2" <?php if($personasCasa=='2') echo 'selected="selected"';  ?>>2</option>
                                <option value="3" <?php if($personasCasa=='3') echo 'selected="selected"';  ?>>3</option><option value="4" <?php if($personasCasa=='4') echo 'selected="selected"';  ?>>4</option>
                                <option value="5" <?php if($personasCasa=='5') echo 'selected="selected"';  ?>>5</option><option value="6" <?php if($personasCasa=='6') echo 'selected="selected"';  ?>>6</option>
                                <option value="7" <?php if($personasCasa=='7') echo 'selected="selected"';  ?>>7</option><option value="8" <?php if($personasCasa=='8') echo 'selected="selected"';  ?>>8</option>
                                <option value="9" <?php if($personasCasa=='9') echo 'selected="selected"';  ?>>9</option><option value="10" <?php if($personasCasa=='10') echo 'selected="selected"';  ?>>10 o más</option>
                            </select>
                        </div>
                        <div>&nbsp;</div>
                        <div class="col-sm-12">
                            <label for="viveEn" class="form-label">Vive en</label>
                            <select class="form-control" name="viveEn" id="viveEn">
                                <option value="CASA PROPIA" <?php if($viveEn=='CASA PROPIA') echo 'selected="selected"';  ?>>Casa propia</option>
                                <option value="CASA RENTADA" <?php if($viveEn=='CASA RENTADA') echo 'selected="selected"';  ?>>Casa rentada</option>
                                <option value="CASA PRESTADA SIN PAGO DE RENTA" <?php if($viveEn=='CASA PRESTADA SIN PAGO DE RENTA') echo 'selected="selected"';  ?>>Casa prestada sin pago de renta</option>
                            </select>
                        </div>
                        <div>&nbsp;</div>
                        <div class="col-sm-12">
                            <label for="viveEn" class="form-label">En caso de ser aceptado y radicar FUERA de la zona metropolitana de Puebla, cuando se desarrollen las actividades presenciales &iquest;qué tipo de residencia/habitación ocuparía?</label>
                            <select class="form-control" name="radicar" id="radicar">
                                <option value="NINGUNA" <?php if($radicar=='NINGUNA') echo 'selected="selected"';  ?>></option>
                                <option value="PROPIA" <?php if($radicar=='PROPIA') echo 'selected="selected"';  ?>>Casa propia</option>
                                <option value="RENTADA" <?php if($radicar=='RENTA') echo 'selected="selected"';  ?>>Casa rentada</option>
                                <option value="PENSION" <?php if($radicar=='PENSION') echo 'selected="selected"';  ?>>Pensión</option>
                                <option value="FAMILIAR" <?php if($radicar=='FAMILIAR') echo 'selected="selected"';  ?>>Casa de un familiar</option>
                                <option value="NO SABE" <?php if($radicar=='NO SABE') echo 'selected="selected"';  ?>>No lo sabe</option>
                            </select>
                        </div>
                        <div>&nbsp;</div>
                        <div class="col-sm-12">
                            <label for="apPaterno" class="form-label">&iquest;Trabaja en el sector salud?: </label>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" id="trabajaSalud1" name="trabajaSalud" value="1" <?php if($trabajaSalud=='1') echo 'checked="checked"';  ?>>
                                <label class="form-check-label" for="trabajaSalud1">Sí</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" id="trabajaSalud2" name="trabajaSalud" value="0" <?php if($trabajaSalud!='1') echo 'checked="checked"';  ?>>
                                <label class="form-check-label" for="trabajaSalud2">No</label>
                            </div>
                        </div>
                        <div>&nbsp;</div>
                        <div class="col-sm-12">
                            <label for="apPaterno" class="form-label">&iquest;Es miembro de una comunidad indígena?: </label>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" id="comunidadIndigena1" name="comunidadIndigena" value="1" <?php if($comunidadIndigena=='1') echo 'checked="checked"';  ?>>
                                <label class="form-check-label" for="comunidadIndigena1">Sí</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" id="comunidadIndigena2" name="comunidadIndigena" value="0" <?php if($comunidadIndigena!='1') echo 'checked="checked"';  ?>>
                                <label class="form-check-label" for="comunidadIndigena2">No</label>
                            </div>
                        </div>
                        <div>&nbsp;</div>
                        <div class="col-sm-12">
                            <label for="apPaterno" class="form-label">&iquest;Se encuentra en situación vulnerable?: </label>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" id="situacionVulnerable1" name="situacionVulnerable" value="1" <?php if($situacionVulnerable=='1') echo 'checked="checked"';  ?>>
                                <label class="form-check-label" for="situacionVulnerable1">Sí</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" id="situacionVulnerable2" name="situacionVulnerable" value="0" <?php if($situacionVulnerable!='1') echo 'checked="checked"';  ?>>
                                <label class="form-check-label" for="situacionVulnerable2">No</label>
                            </div>
                            <input type="text" class="form-control" name="vulnerableCual" value="<?php echo $vulnerableCual ?>" placeholder="Indique cual en caso de responder que Sí" />
                        </div>
                        <div>&nbsp;</div>
                        <div class="col-sm-12">
                            <label for="apPaterno" class="form-label">
                                &iquest;En el año 2020 participó en algún proceso de admisión para el área de la salud? Si su respuesta es si, especifique la universidad y licenciatura solicitada.
                            </label>
                            <input type="text" class="form-control" id="txtapPaterno" name="admisionPrevio" value="<?php echo $admisionPrevio ?>"  />
                        </div>
                        <div class="col-sm-12">
                            <label for="apPaterno" class="form-label">
                                &iquest;En este año  participará en algún proceso de admisión para el área de la salud? Si su respuesta es si, especifique la universidad y licenciatura solicitada.
                            </label>
                            <input type="text" class="form-control" id="txtapPaterno" name="admisionActual" value="<?php echo $admisionActual ?>"  />
                        </div>
                        <div>&nbsp;</div>
                        <div class="col-sm-12">
                            <label for="apPaterno" class="form-label">&iquest;Tiene computadora de escritorio?: </label>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" id="tieneDesktop1" name="tieneDesktop" value="1" <?php if($tieneDesktop=='1') echo 'checked="checked"';  ?>>
                                <label class="form-check-label" for="tieneDesktop1">Sí</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" id="tieneDesktop2" name="tieneDesktop" value="0" <?php if($tieneDesktop!='1') echo 'checked="checked"';  ?>>
                                <label class="form-check-label" for="tieneDesktop2">No</label>
                            </div>
                        </div>
                        <div>&nbsp;</div>
                        <div class="col-sm-12">
                            <label for="apPaterno" class="form-label">&iquest;Tiene computadora portátil (laptop)?: </label>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" id="tieneLaptop2" name="tieneLaptop" value="1" <?php if($tieneLaptop=='1') echo 'checked="checked"';  ?>>
                                <label class="form-check-label" for="tieneLaptop2">Sí</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" id="tieneLaptop1" name="tieneLaptop" value="0" <?php if($tieneLaptop!='1') echo 'checked="checked"';  ?>>
                                <label class="form-check-label" for="tieneLaptop1">No</label>
                            </div>
                        </div>
                        <div>&nbsp;</div>
                        <div class="col-sm-12">
                            <label for="apPaterno" class="form-label">Si cuenta con alguna de las dos, &iquest;su computadora cuenta con micrófono y cámara?: </label>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" id="tieneWebcam1" name="tieneWebcam" value="1" <?php if($tieneWebcam=='1') echo 'checked="checked"';  ?>>
                                <label class="form-check-label" for="tieneWebcam1">Sí</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" id="tieneWebcam2" name="tieneWebcam" value="0" <?php if($tieneWebcam!='1') echo 'checked="checked"';  ?>>
                                <label class="form-check-label" for="tieneWebcam2">No</label>
                            </div>
                        </div>
                        <div>&nbsp;</div>
                        <div class="col-sm-12">
                            <label for="apPaterno" class="form-label">En caso de no contar con computadora con cámara y micrófono, &iquest;Tiene la posibilidad de conseguir una para la aplicación de su examen? </label>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" id="conseguirCompu1" name="conseguirCompu" value="1" <?php if($conseguirCompu=='1') echo 'checked="checked"';  ?>>
                                <label class="form-check-label" for="conseguirCompu1">Sí</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" id="conseguirCompu2" name="conseguirCompu" value="0" <?php if($conseguirCompu!='1') echo 'checked="checked"';  ?>>
                                <label class="form-check-label" for="conseguirCompu2">No</label>
                            </div>
                        </div>
                        <div>&nbsp;</div>
                        
                        <div class="col-sm-12">
                            <label for="apPaterno" class="form-label">&iquest;Tiene servicio de internet?: </label>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" id="tieneInternet1" name="tieneInternet" value="1" <?php if($tieneInternet=='1') echo 'checked="checked"';  ?>>
                                <label class="form-check-label" for="tieneInternet1">Sí</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" id="tieneInternet2" name="tieneInternet" value="0" <?php if($tieneInternet!='1') echo 'checked="checked"';  ?>>
                                <label class="form-check-label" for="tieneInternet2">No</label>
                            </div>
                        </div>
                        <div>&nbsp;</div>
                        <div class="col-sm-12">
                            <label for="apPaterno" class="form-label">&iquest;Tiene tableta inteligente (tablet)?: </label>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" id="tieneTablet1" name="tieneTablet" value="1" <?php if($tieneTablet=='1') echo 'checked="checked"';  ?>>
                                <label class="form-check-label" for="tieneTablet1">Sí</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" id="tieneTablet2" name="tieneTablet" value="0" <?php if($tieneTablet!='1') echo 'checked="checked"';  ?>>
                                <label class="form-check-label" for="tieneTablet2">No</label>
                            </div>
                        </div>
                        <div>&nbsp;</div>
                        <div class="col-sm-12">
                            <label for="apPaterno" class="form-label">&iquest;Tiene teléfono inteligente (smartphone)?: </label>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" id="tieneSmartphone1" name="tieneSmartphone" value="1" <?php if($tieneSmartphone=='1') echo 'checked="checked"';  ?>>
                                <label class="form-check-label" for="tieneSmartphone1">Sí</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" id="tieneSmartphone2" name="tieneSmartphone" value="0" <?php if($tieneSmartphone!='1') echo 'checked="checked"';  ?>>
                                <label class="form-check-label" for="tieneSmartphone2">No</label>
                            </div>
                        </div>
                        <div>&nbsp;</div>
                        <div class="col-sm-12">
                            <label for="personasCasa" class="form-label">En caso de que tenga alguna beca, por favor indique cual.</label>
                            <select name="beca" class="form-control">
                                <option value="NINGUNA" <?php if($beca=='NINGUNA') echo 'selected="selected"';  ?>>No cuento con beca</option>
                                <option value="MANUTENCION" <?php if($beca=='MANUTENCION') echo 'selected="selected"';  ?>>Beca de manutención</option>
                                <option value="JOVENES ESCRIBIENDO EL FUTURO" <?php if($beca=='JOVENES ESCRIBIENDO EL FUTURO') echo 'selected="selected"';  ?>>Jóvenes escribiendo el futuro</option>
                                <option value="ELISA ACUÑA" <?php if($beca=='ELISA ACUÑA') echo 'selected="selected"';  ?>>Elsa Acuña</option>
                                <option value="OTRA" <?php if($beca=='OTRA') echo 'selected="selected"';  ?>>Otra</option>
                            </select>
                        </div>
                        <div>&nbsp;</div>
                        <div class="col-sm-12" style="display: none;">
                            <label for="apPaterno" class="form-label">Indique cual es su preferencia para el examen de admisión: </label>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" id="examenOnline1" name="examenOnline" value="1" <?php if($examenOnline=='1') echo 'checked="checked"';  ?>>
                                <label class="form-check-label" for="examenOnline1">En línea desde casa</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" id="examenOnline2" name="examenOnline" value="0" <?php if($examenOnline!='1') echo 'checked="checked"';  ?>>
                                <label class="form-check-label" for="examenOnline2">Presencial en alguna sede de aplicación</label>
                            </div>
                        </div>
                        <div>&nbsp;</div>
                        
                        
                        <?php echo $this->include('perfil/botonesguardar'); ?>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


<?php echo $this->endSection() ?>