<div>
    <div>
        <table width="100%">
            <tr>
                <td >
                    <img src="<?php echo base_url('img/logos/Gobierno.png'); ?>" />
                </td>
                <td >
                    <img src="<?php echo base_url('img/logos/SEP.png'); ?>" />
                </td>
                <td >
                    <img src="<?php echo base_url('img/logos/USEP.png'); ?>" />
                </td>
            </tr>
        </table>
    </div>

    <div style="text-align: center; font-weight: bold;">
        RESULTADOS DEL EXAMEN DE ADMISIÓN 2021
    </div>

    <div style="margin-bottom: 20px; text-align: center;">
        Estimado <?php echo "$nombre $apPaterno $apMaterno"; ?>:
    </div>

    <div style="margin-bottom: 20px;">
        El puntaje obtenido en el examen de admisión 2021 <strong>no ha sido suficiente</strong>
        para obtener un lugar en la Universidad de la Salud del Estado de Puebla.
    </div>

    <div style="margin-bottom: 20px;">
        Reconocemos tu esfuerzo y te invitamos a continuar preparándote.
    </div>

    <div style="text-align: center; font-weight: bold; margin-bottom: 20px;">
        PUNTAJE OBTENIDO: <?php echo number_format( $puntaje, 0, '.', ',') ?>
    </div>

    <div style="background-image: url('<?php echo base_url('img/plecas/carta_aceptacion_footer.png'); ?>'); background-size: 100%; height: 115px; background-repeat: no-repeat;">
        
    </div>
</div>