<?php echo $this->extend('plantilla_menu'); ?>

<?php echo $this->section('workarea') ?>


<div>&nbsp;</div>
<?php echo $this->include('perfil/progreso'); ?>
<div>&nbsp;</div>

<div class="container-fluid">
    <div class="row">
        
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                    <strong>Proceso de registro concluido</strong>
                </div>
                <div class="card-body">

                    <p>El periodo para el registro al examen de admisión USEP 2021 y atención de observaciones, inició el 10 de mayo y finalizó el 4 de junio del 2021.</p>
                    <div class="alert alert-danger">
                        <p>
                            <?php if( $captura ): ?>
                                Usted no concluyó la captura de su información.
                            <?php endif; ?>
                        </p>
                        <p>
                            <?php if( $promedio ): ?>
                                Su promedio no cumple con el estupilado en la convocatoria, ya que el registro y documentos indican que fue de <?php echo $promedio; ?>
                            <?php endif; ?>
                        </p>
                        <?php if(isset($observaciones) && count($observaciones)): ?>
                            <p>Tiene observaciones no atendidas en sus documentos:</p>
                            <?php foreach( $observaciones as $observacion ): ?>
                                <?php echo $observacion; ?><br />
                            <?php endforeach; ?>
                            
                        <?php endif; ?>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</div>

<?php echo $this->endSection() ?>