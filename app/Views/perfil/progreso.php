<div class="container-fluid">
    <div class="col-sm-12">
        <div class="card ">
            <div class="card-header">
                <strong>Status</strong>
            </div>
            <div class="card-body">
                <div class="progress" style="height: 30px;">
                    <?php if($progreso['global']['registro'] == '1' ): ?>
                        <div class="progress-bar progress-bar-striped progress-bar-animated bg-secondary" role="progressbar" style="width: 20%" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                            Inicio
                        </div>
                    <?php endif; ?>

                    <?php if($progreso['global']['validacion'] == '1' ): ?>
                        <div class="progress-bar progress-bar-striped progress-bar-animated bg-secondary" role="progressbar" style="width: 20%" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                            Validación de correo
                        </div>
                    <?php endif; ?>

                    <?php if($progreso['global']['captura'] == '1' ): ?>
                        <div class="progress-bar progress-bar-striped progress-bar-animated bg-secondary" role="progressbar" style="width: 20%" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                            Captura de información
                        </div>
                    <?php endif; ?>

                    <?php if($progreso['global']['documentos'] == '1' ): ?>
                        <div class="progress-bar progress-bar-striped progress-bar-animated bg-secondary" role="progressbar" style="width: 20%" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                            Carga de documentos
                        </div>
                    <?php endif; ?>

                    <?php if( $progreso['global']['status'] == 'CORRECTO' ): ?>
                        <div class="progress-bar progress-bar-striped progress-bar-animated bg-secondary" role="progressbar" style="width: 20%" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                            Finalizado
                        </div>
                    <?php endif; ?>
                </div>
                <!-- 
                <div class="col-sm-12">
                    <?php 
                        if ( $progreso['global']['captura'] == '0' ) { ?>
                            Actividad actual: <strong>Captura de información</strong><br />Actividad siguiente: <em>Carga de documentos</em> <?php
                        }
                        else if ( $progreso['global']['captura'] == '1' && $progreso['global']['documentos'] == '0' ) { ?>
                            Actividad actual: <strong>Carga de documentos</strong><br />Actividad siguiente: <em>Validación de documentos</em> <?php
                        }
                        else if ( $progreso['global']['captura'] == '1' && $progreso['global']['documentos'] == '1' && $progreso['global']['revision'] == '0' ) { ?>
                            Actividad actual: <strong>Validación de documentos</strong><br />Actividad siguiente: <em>Ficha de Examen</em> <?php
                        }
                        else {
                            if ( $progreso['global']['status'] == 'CORRECTO' ) { ?>
                                &iexcl;Felicidades! La validación de tus documentos se ha realizado correctamente.
                                Podrás descargar tu ficha de examen del 17 al 21 de junio.<?php
                            }
                        }
                    ?>
                </div>
                <?php if( $progreso['global']['status'] != 'CORRECTO' && $progreso['global']['revision'] == '1' && ( $progreso['global']['exito'] == '1' ||  $progreso['global']['declinado'] == '1' ) ): ?>
                    <div class="alert alert-danger">
                        Tu expediente tiene observaciones. Por favor ve a la sección de Carga de documentos para realizar las correcciones.
                    </div>
                <?php endif; ?>

                <?php if( $progreso['global']['rechazado'] === '1' ): ?>
                    <div class="alert alert-danger">
                        No puedes continuar en el proceso debido a que tu promedio no es igual o mayor al publicado en la convocatoria.<br />
                        Si deseas aclarar esta situación, por favor escribe un correo electrónico a
                        <a href="maito:admision@usep.mx" class="alert-link">admision@usep.mx</a>
                    </div>
                <?php endif; ?>
                -->
            </div>
            
        </div>
    </div>
</div>