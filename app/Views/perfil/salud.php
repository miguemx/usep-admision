<?php echo $this->extend('plantilla_menu'); ?>

<?php echo $this->section('workarea') ?>


<div>&nbsp;</div>
<?php echo $this->include('perfil/progreso'); ?>
<div>&nbsp;</div>

<div class="container-fluid">
    <div class="row">
        
        <?php echo $this->include('menus/lateralaspirantes'); ?>
        
        <div class="col-sm-9">
            <div class="card">
                <div class="card-header">
                    <strong>Datos de Salud</strong>
                </div>
                <div class="card-body">
                    <?php if(isset($guardado) ): ?>
                        <div class="alert alert-success">
                            Tu información ha sido guardada.
                        </div>
                    <?php endif; ?>
                    <?php if(isset($errors) ): ?>
                        <div class="alert alert-danger">
                            Ocurrieron los siguientes errores:<br />
                            <?php foreach($errors as $error): ?>
                                <?php echo $error; ?><br />
                            <?php endforeach; ?>
                        </div>
                    <?php endif; ?>
                    <form action="<?php echo base_url('Perfil/Salud') ?>" method="post">
                        <div class="col-sm-12">
                            <label for="carrera" class="form-label">Tipo de Sangre</label>
                            <select class="form-control" name="tipoSangre">
                                <option value="O+" <?php if($tipoSangre=='O+') echo 'selected="selected"';  ?>>O+ (O positivo)</option>
                                <option value="O-" <?php if($tipoSangre=='O-') echo 'selected="selected"';  ?>>O- (O negativo)</option>
                                <option value="A+" <?php if($tipoSangre=='A+') echo 'selected="selected"';  ?>>A+ (A positivo)</option>
                                <option value="A-" <?php if($tipoSangre=='A-') echo 'selected="selected"';  ?>>A- (A negativo)</option>
                                <option value="B+" <?php if($tipoSangre=='B+') echo 'selected="selected"';  ?>>B+ (B positivo)</option>
                                <option value="B-" <?php if($tipoSangre=='B-') echo 'selected="selected"';  ?>>B- (B negativo)</option>
                                <option value="AB+" <?php if($tipoSangre=='AB+') echo 'selected="selected"';  ?>>AB+ (AB positivo)</option>
                                <option value="AB-" <?php if($tipoSangre=='AB-') echo 'selected="selected"';  ?>>AB- (AB negativo)</option>
                            </select>
                        </div>
                        <div>&nbsp;</div>
                        <div class="col-sm-12">
                            <label for="apPaterno" class="form-label">Describa si tiene algún o algunos padecimientos</label>
                            <textarea class="form-control" name="padecimientos"><?php echo $padecimientos; ?></textarea>
                        </div>
                        <div>&nbsp;</div>
                        <div class="col-sm-12">
                            <label for="apMaterno" class="form-label">Describa si tiene algún tipo de discapacidad</label>
                            <input type="text" class="form-control" id="txtapMaterno" name="discapacidad" value="<?php echo $discapacidad; ?>" />
                        </div>
                        <div>&nbsp;</div>
                        <div class="col-sm-12">
                            <label for="apPaterno" class="form-label">&iquest;Cuentas con certificado médico de discapacidad?: </label>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" id="certificadoDiscapacidad1" name="certificadoDiscapacidad" value="1" <?php if($certificadoDiscapacidad=='1') echo 'checked="checked"';  ?>>
                                <label class="form-check-label" for="certificadoDiscapacidad1">Sí</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" id="certificadoDiscapacidad2" name="certificadoDiscapacidad" value="0" <?php if($certificadoDiscapacidad!='1') echo 'checked="checked"';  ?>>
                                <label class="form-check-label" for="certificadoDiscapacidad2">No</label>
                            </div>
                        </div>
                        <div>&nbsp;</div>
                        <div class="col-sm-12">
                            <label for="apPaterno" class="form-label">&iquest;Usa lentes?: </label>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" id="usaLentes1" name="usaLentes" value="1" <?php if($usaLentes=='1') echo 'checked="checked"';  ?>>
                                <label class="form-check-label" for="usaLentes1">Sí</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" id="usaLentes2" name="usaLentes" value="0" <?php if($usaLentes!='1') echo 'checked="checked"';  ?>>
                                <label class="form-check-label" for="usaLentes2">No</label>
                            </div>
                        </div>
                        <div>&nbsp;</div>
                        <div class="col-sm-12">
                            <label for="apPaterno" class="form-label">&iquest;Tiene alguna prótesis?: </label>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" id="tieneProtesis1" name="tieneProtesis" value="1" <?php if($tieneProtesis=='1') echo 'checked="checked"';  ?>>
                                <label class="form-check-label" for="tieneProtesis1">Sí</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" id="tieneProtesis2" name="tieneProtesis" value="0" <?php if($tieneProtesis!='1') echo 'checked="checked"';  ?>>
                                <label class="form-check-label" for="tieneProtesis2">No</label>
                            </div>
                        </div>
                        <div>&nbsp;</div>
                        <div class="col-sm-12">
                            <label for="apMaterno" class="form-label">&iquest;Con cual servicio de salud cuenta?</label>
                            <select class="form-control" name="servicioMedico">
                                <option value="NINGUNO" <?php if($servicioMedico=='NINGUNO') echo 'selected="selected"';  ?>>Ninguno</option>
                                <option value="IMSS" <?php if($servicioMedico=='IMSS') echo 'selected="selected"';  ?>>IMSS</option>
                                <option value="ISSSTE" <?php if($servicioMedico=='ISSSTE') echo 'selected="selected"';  ?>>ISSSTE</option>
                                <option value="ISSSTEP" <?php if($servicioMedico=='ISSSTEP') echo 'selected="selected"';  ?>>ISSSTEP</option>
                                <option value="INSABI" <?php if($servicioMedico=='INSABI') echo 'selected="selected"';  ?>>INSABI</option>
                                <option value="Otro" <?php if($servicioMedico=='Otro') echo 'selected="selected"';  ?>>Otro</option>
                            </select>
                        </div>
                        <?php echo $this->include('perfil/botonesguardar'); ?>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


<?php echo $this->endSection() ?>