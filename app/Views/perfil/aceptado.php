<?php echo $this->extend('plantilla_menu'); ?>

<?php echo $this->section('workarea') ?>


<div>&nbsp;</div>
<?php echo $this->include('perfil/progreso'); ?>
<div>&nbsp;</div>

<div class="container-fluid">
    <div class="row">
        
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                    <strong>Proceso de registro concluido</strong>
                </div>
                <div class="card-body">
                    <p>El periodo para el registro al examen de admisión USEP 2021 y atención de observaciones, inició el 10 de mayo y finalizó el 4 de junio del 2021.</p>
                    <div class="alert alert-success">
                        <p>Podrás descargar tu ficha de examen en el siguiente enlace:</p>
                        <p>
                            <a href="<?php echo base_url('Perfil/FichaExamen') ?>" class="btn btn-secondary" target="_blank">Ficha de Examen</a><br />
                            <!-- <a href="#" class="btn btn-secondary">Ficha de Examen</a><br />
                            <em>Disponible a partir de las 20:00 horas del 17 de junio del 2021.</em> -->
                            <em>El periodo de descarga de ficha de examen ha concluido.</em>
                        </p>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</div>

<?php echo $this->endSection() ?>