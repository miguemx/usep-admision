<div>&nbsp;</div>
<?php if ($enviado=='1'): ?>
    <div class="alert alert-secondary">Los datos de esta sección ya han sido enviados.</div>
<?php else: ?>
    <button type="submit" class="btn btn-secondary" name="ged">Guardar y terminar después</button>
    <button type="submit" class="btn btn-secondary" name="gf">Guardar y continuar</button>
    <div>Entiendo que al dar clic en <strong>Guardar y continuar</strong> acepto que mis datos son verdaderos y no podré modificarlos posteriormente</div>
<?php endif; ?>