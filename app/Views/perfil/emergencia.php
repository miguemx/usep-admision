<?php echo $this->extend('plantilla_menu'); ?>

<?php echo $this->section('workarea') ?>


<div>&nbsp;</div>
<?php echo $this->include('perfil/progreso'); ?>
<div>&nbsp;</div>

<div class="container-fluid">
    <div class="row">
        
        <?php echo $this->include('menus/lateralaspirantes'); ?>
        
        <div class="col-sm-9">
            <div class="card">
                <div class="card-header">
                    <strong>Contacto en caso de emergencia</strong>
                </div>
                <div class="card-body">
                    <?php if(isset($guardado) ): ?>
                        <div class="alert alert-success">
                            Tu información ha sido guardada.
                        </div>
                    <?php endif; ?>
                    <?php if(isset($errors) ): ?>
                        <div class="alert alert-danger">
                            Ocurrieron los siguientes errores:<br />
                            <?php foreach($errors as $error): ?>
                                <?php echo $error; ?><br />
                            <?php endforeach; ?>
                        </div>
                    <?php endif; ?>
                    <form action="<?php echo base_url('Perfil/Emergencia') ?>" method="post">
                        <div class="col-sm-12">
                            <label for="carrera" class="form-label">Nombre completo:</label>
                            <input type="text" class="form-control" id="txtcarrera" name="nombre" value="<?php echo $nombre; ?>" />
                        </div>
                        
                        <div class="col-sm-12">
                            <label for="carrera" class="form-label">Parentesco</label>
                            <select class="form-control" name="parentesco">
                                <option value="PADRE"  <?php if($parentesco=='PADRE') echo 'selected="selected"';  ?>>Padre</option>
                                <option value="MADRE" <?php if($parentesco=='MADRE') echo 'selected="selected"';  ?>>Madre</option>
                                <option value="CONYUGE <?php if($parentesco=='CONYUGE') echo 'selected="selected"';  ?>">Cónyuge</option>
                                <option value="HERMANO" <?php if($parentesco=='HERMANO') echo 'selected="selected"';  ?>>Hermano(a)</option>
                                <option value="ABUELOS" <?php if($parentesco=='ABUELOS') echo 'selected="selected"';  ?>>Abuelo(a)</option>
                                <option value="TIOS" <?php if($parentesco=='TIOS') echo 'selected="selected"';  ?>>Tío(a)</option>
                                <option value="PRIMOS" <?php if($parentesco=='PRIMOS') echo 'selected="selected"';  ?>>Primo(a)</option>
                                <option value="SOBRINOS" <?php if($parentesco=='SOBRINOS') echo 'selected="selected"';  ?>>Sobrino(a)</option>
                                <option value="HIJOS" <?php if($parentesco=='HIJOS') echo 'selected="selected"';  ?>>Hijo(a)</option>
                                <option value="OTRO" <?php if($parentesco=='OTRO') echo 'selected="selected"';  ?>>Otro</option>
                            </select>
                        </div>
                        <div class="col-sm-12">
                            <label for="carrera" class="form-label">Teléfono</label>
                            <input type="text" class="form-control" id="txtcarrera" name="telefono" value="<?php echo $telefono; ?>" />
                        </div>
                        <div class="col-sm-12">
                            <label for="carrera" class="form-label">Correo electrónico</label>
                            <input type="text" class="form-control" id="txtcarrera" name="correo" value="<?php echo $correo; ?>" />
                        </div>
                        
                        <?php echo $this->include('perfil/botonesguardar'); ?>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


<?php echo $this->endSection() ?>