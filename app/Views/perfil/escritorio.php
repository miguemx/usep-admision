<?php echo $this->extend('plantilla_menu'); ?>

<?php echo $this->section('workarea') ?>


<div>&nbsp;</div>
<?php echo $this->include('perfil/progreso'); ?>
<div>&nbsp;</div>

<div class="container-fluid">
    <div class="row">
        
        <?php echo $this->include('menus/lateralaspirantes'); ?>
        
        <div class="col-sm-9">
            <div class="card">
                <div class="card-header">
                    <strong>Datos Generales</strong>
                </div>
                <div class="card-body">
                    <?php if(isset($guardado) ): ?>
                        <div class="alert alert-success">
                            Tu información ha sido guardada.
                        </div>
                    <?php endif; ?>
                    <?php if(isset($errors) ): ?>
                        <div class="alert alert-danger">
                            Ocurrieron los siguientes errores:<br />
                            <?php foreach($errors as $error): ?>
                                <?php echo $error; ?><br />
                            <?php endforeach; ?>
                        </div>
                    <?php endif; ?>
                    <form action="<?php echo base_url('Perfil'); ?>" method="post">
                        <div class="col-sm-12">
                            <label for="carrera" class="form-label">Opción de ingreso</label>
                            <select class="form-control" name="carrera" disabled="disabled">
                                <option value="1" <?php if($carrera=='1') echo 'selected="selected"';  ?>>LICENCIATURA EN ENFERMERÍA Y OBSTETRICIA</option>
                                <option value="2" <?php if($carrera=='2') echo 'selected="selected"';  ?>>LICENCIATURA EN MÉDICO CIRUJANO</option>
                            </select>
                        </div>
                        <div class="col-sm-12">
                            <label for="apPaterno" class="form-label">Apellido Paterno</label>
                            <input type="text" class="form-control" id="txtapPaterno" value="<?php echo $apPaterno ?>" disabled="disabled" />
                        </div>
                        <div class="col-sm-12">
                            <label for="apMaterno" class="form-label">Apellido Materno</label>
                            <input type="text" class="form-control" id="txtapMaterno" value="<?php echo $apMaterno ?>" disabled="disabled" />
                        </div>
                        <div class="col-sm-12">
                            <label for="nombre" class="form-label">Nombre</label>
                            <input type="text" class="form-control" id="txtnombre" value="<?php echo $nombre ?>" disabled="disabled" />
                        </div>
                        <div class="col-sm-12">
                            <label for="curp" class="form-label">CURP</label>
                            <input type="text" class="form-control" id="txtcurp" value="<?php echo $curp ?>" disabled="disabled" />
                        </div>
                        <div class="col-sm-12">
                            <label for="correo" class="form-label">Correo electrónico</label>
                            <input type="text" class="form-control" id="txtcorreo" value="<?php echo $correo ?>" disabled="disabled" />
                        </div>
                        <div class="col-sm-12">
                            <label for="telefono" class="form-label">Teléfono</label>
                            <input type="text" class="form-control" id="txttelefono" value="<?php echo $telefono ?>" disabled="disabled" />
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                <label for="calle" class="form-label">Calle</label>
                                <input type="text" class="form-control" id="calle" name="calle" value="<?php echo $calle ?>" <?php if($enviado=='1') echo 'disabled="disabled"'; ?> />
                            </div>
                            <div class="col-sm-3">
                                <label for="numero" class="form-label">Núm.</label>
                                <input type="text" class="form-control" id="numero" name="numero" value="<?php echo $numero ?>" <?php if($enviado=='1') echo 'disabled="disabled"'; ?> />
                            </div>
                            <div class="col-sm-3">
                                <label for="numeroInterior" class="form-label">Interior</label>
                                <input type="text" class="form-control" id="numeroInterior" name="numeroInterior" value="<?php echo $numeroInterior ?>" <?php if($enviado=='1') echo 'disabled="disabled"'; ?> />
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-8">
                                <label for="colonia" class="form-label">Colonia</label>
                                <input type="text" class="form-control" id="colonia" name="colonia" value="<?php echo $colonia ?>" <?php if($enviado=='1') echo 'disabled="disabled"'; ?> />
                            </div>
                            <div class="col-sm-4">
                                <label for="cp" class="form-label">C.P.</label>
                                <input type="text" class="form-control" id="cp" name="cp" value="<?php echo $cp ?>" <?php if($enviado=='1') echo 'disabled="disabled"'; ?> />
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <label for="pais" class="form-label">País</label>
                            <select name="pais" class="form-control" <?php if($enviado=='1') echo 'disabled="disabled"'; ?> id="cmbPais" onchange="cambiaPais()" >
                                <option value="MEXICO" <?php if($pais=='MEXICO') echo 'selected="selected"'; ?> >MEXICO</option>
                                <option value="EXTRANJERO" <?php if($pais=='EXTRANJERO') echo 'selected="selected"'; ?> >Vivo en el extranjero</option>
                            </select>
                        </div>
                        <div class="col-sm-12">
                            <label for="estado" class="form-label">Estado</label>
                            <div>
                                <select name="estado" class="form-control" id="cmbEstados" onchange="obtieneMunicipios('cmbEstados','cmbMunicipios')" <?php if($enviado=='1') echo 'disabled="disabled"'; ?>>
                                    <option value="">Selecciona un estado</option>
                                    <?php foreach( $estados as $edo ): ?>
                                        <option value="<?php echo $edo->nombre ?>" <?php if( $estado == $edo->nombre ) echo 'selected="selected"'; ?> ><?php echo $edo->nombre; ?></option>
                                    <?php endforeach; ?>
                                    <option value="EXTRANJERO" <?php if( $estado == 'EXTRANJERO' ) echo 'selected="selected"'; ?>>Vivo en el Extranjero</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <label for="municipio" class="form-label">Municipio</label>
                            <select name="municipio" class="form-control" id="cmbMunicipios" <?php if($enviado=='1') echo 'disabled="disabled"'; ?>>
                                <option value="">Cargando...</option>
                                <option value="EXTRANJERO" <?php if( $municipio == 'EXTRANJERO' ) echo 'selected="selected"'; ?>>Vivo en el Extranjero</option>
                            </select>
                        </div>
                        
                        
                        <div class="col-sm-12">
                            <label for="nacimientoFecha" class="form-label">Fecha de nacimiento</label>
                            <input type="date" class="form-control" id="nacimientoFecha" name="nacimientoFecha" value="<?php echo $nacimientoFecha ?>" <?php if($enviado=='1') echo 'disabled="disabled"'; ?> />
                        </div>
                        <div class="col-sm-12">
                            <label for="nacimientoPais" class="form-label">País de nacimiento</label>
                            <select name="nacimientoPais" class="form-control" id="cmbPaisNacimiento" onchange="cambiaPais('Nacimiento')" <?php if($enviado=='1') echo 'disabled="disabled"'; ?>>
                                <option value="MEXICO" <?php if($nacimientoPais=='MEXICO') echo 'selected="selected"'; ?> >MEXICO</option>
                                <option value="EXTRANJERO" <?php if($nacimientoPais=='EXTRANJERO') echo 'selected="selected"'; ?> >Nací en el extranjero</option>
                            </select>
                        </div>
                        <div class="col-sm-12">
                            <label for="nacimientoEstado" class="form-label">Estado de nacimiento</label>
                            <select name="nacimientoEstado" class="form-control" id="cmbEstadosNacimiento" onchange="obtieneMunicipios('cmbEstadosNacimiento','cmbMunicipiosNacimiento')" <?php if($enviado=='1') echo 'disabled="disabled"'; ?>>
                                <option value="">Selecciona un estado</option>
                                <?php foreach( $estados as $edo ): ?>
                                    <option value="<?php echo $edo->nombre ?>" <?php if( $nacimientoEstado == $edo->nombre ) echo 'selected="selected"'; ?> ><?php echo $edo->nombre; ?></option>
                                <?php endforeach; ?>
                                <option value="EXTRANJERO" <?php if( $nacimientoEstado == 'EXTRANJERO' ) echo 'selected="selected"'; ?>>Nací en el Extranjero</option>
                            </select>
                        </div>
                        <div class="col-sm-12">
                            <label for="nacimientoMunicipio" class="form-label">Municipio de nacimiento</label>
                            <select name="nacimientoMunicipio" class="form-control" id="cmbMunicipiosNacimiento" <?php if($enviado=='1') echo 'disabled="disabled"'; ?>>
                                <option value="">Cargando...</option>
                            </select>
                        </div>
                        
                        
                        
                        <?php echo $this->include('perfil/botonesguardar'); ?>
                        
                        
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    function cambiaPais(suf) {
        if ( !suf ) suf = '';
        let cmbPais = document.getElementById('cmbPais' + suf);
        if ( cmbPais.value == 'EXTRANJERO' ) {
            document.getElementById('cmbEstados' + suf).value="EXTRANJERO";
            document.getElementById('cmbMunicipios' + suf).value="EXTRANJERO";
        }
        else {
            document.getElementById('cmbEstados' + suf).value="";
            document.getElementById('cmbMunicipios' + suf).value="";
        }
    }

    var obtieneMunicipios = async (origen,destino) => {
        let estado = document.getElementById( origen ).value;
        const formData = new FormData();

        formData.append('estado', estado);
        document.getElementById(destino).innerHTML = '';
        let response = await fetch('<?php echo base_url("consultas/getmunicipios") ?>', {
            method: 'POST',
            body: formData,
        }).then(response => response.json()).then(result => {
                return result;
            }).catch(error => {
                console.error('Error:', error);
            });

        document.getElementById(destino).innerHTML = '';

        for ( let i=0; i<response.length; i++ ) {
            let option = document.createElement('option');
            option.value = response[i].nombre;
            option.innerHTML = response[i].nombre;
            document.getElementById(destino).appendChild(option);
        }

        let option = document.createElement('option');
        option.value = 'NA';
        option.innerHTML = 'Extranjero';
        document.getElementById(destino).appendChild(option);

        option = document.createElement('option');
        option.value = '';
        option.innerHTML = ' ';
        document.getElementById(destino).appendChild(option);

        document.getElementById('cmbMunicipios').value = '<?php echo $municipio; ?>';
        document.getElementById('cmbMunicipiosNacimiento').value = '<?php echo $nacimientoMunicipio; ?>';
    }

    obtieneMunicipios( 'cmbEstados', 'cmbMunicipios' );
    obtieneMunicipios('cmbEstadosNacimiento','cmbMunicipiosNacimiento');

    
</script>


<?php echo $this->endSection() ?>