<?php echo $this->extend('plantilla_menu'); ?>

<?php echo $this->section('workarea') ?>


<div>&nbsp;</div>
<?php echo $this->include('perfil/progreso'); ?>
<div>&nbsp;</div>

<div class="container-fluid">
    <div class="row">
        
        <?php echo $this->include('menus/lateralaspirantes'); ?>
        
        <div class="col-sm-9">
            <div class="card">
                <div class="card-header">
                    <strong>Institución de educación media superior de procedencia</strong>
                </div>
                <div class="card-body">
                    <?php if(isset($guardado) ): ?>
                        <div class="alert alert-success">
                            Tu información ha sido guardada.
                        </div>
                    <?php endif; ?>
                    <?php if(isset($errors) ): ?>
                        <div class="alert alert-danger">
                            Ocurrieron los siguientes errores:<br />
                            <?php foreach($errors as $error): ?>
                                <?php echo $error; ?><br />
                            <?php endforeach; ?>
                        </div>
                    <?php endif; ?>
                    <form action="<?php echo base_url('Perfil/Procedencia'); ?>" method="post">
                        
                        <div class="col-sm-12">
                            <label for="carrera" class="form-label">País</label>
                            <select name="pais" class="form-control" id="cmbPaises" onchange="cambiaEstados()"> 
                                <option value="MEXICO" <?php if( $pais == 'MEXICO') echo 'selected="selected"' ?>>MÉXICO</option>
                                <option value="EXTRANJERO" <?php if( $pais == 'EXTRANJERO') echo 'selected="selected"' ?>>Estudié en el extranjero</option>
                            </select>
                        </div>
                        <div class="col-sm-12">
                            <label for="carrera" class="form-label">Estado</label>
                            <select name="estado" class="form-control" id="cmbEstados" onchange="cambiaMunicipios()">
                                <option value="">Selecciona un estado</option>
                                <?php foreach( $estados as $edo ): ?>
                                    <option value="<?php echo $edo ?>" <?php if($edo==$estado) echo 'selected="selected"'; ?>><?php echo $edo; ?></option>
                                <?php endforeach; ?>
                                <option value="EXTRANJERO" <?php if($estado=='EXTRANJERO') echo 'selected="selected"'; ?>>Extranjero</option>
                            </select>
                        </div>
                        <div class="col-sm-12">
                            <label for="carrera" class="form-label">Municipio</label>
                            <select name="municipio" class="form-control" id="cmbMunicipios" onchange="cambiaEscuelas()">
                                <option value="1">Selecciona un municipio</option>
                            </select>
                        </div>
                        <div class="col-sm-12">
                            <label for="carrera" class="form-label">Escuelas</label>
                            <select  class="form-control" id="cmbEscuelas" onchange="llenaEscuela()">
                                <option value="1">Selecciona tu escuela para completar el formulario</option>
                            </select>
                            <div id="emailHelp" class="form-text">
                                Si no te aparece la lista de escuelas, selecciona un municipio distinto y vuelve a seleccionar 
                                el municipio correcto. Si ves los datos de tu escuela abajo, no es necesario seleccionar nada aquí.</div>
                        </div>
                        <div>&nbsp;</div>
                        <div class="row">
                            <div class="col-sm-8">
                                <label for="carrera" class="form-label">Clave del Centro de Trabajo </label>
                                <input type="text" class="form-control" id="txtCct" name="cct"   value="<?php echo $cct; ?>" />
                                <div id="emailHelp" class="form-text">Verifica que corresponda con el que aparece en tu constancia de estudios, o de lo contrario puedes corregirlo.</div>
                            </div>
                            
                            <div class="col-sm-4">
                                <label for="carrera" class="form-label"> Tipo</label>
                                <select class="form-control" name="tipo" id="txtTipo" >
                                    <option value="PUBLICA" <?php if($tipo=='PUBLICA') echo 'selected="selected"';  ?>>Pública</option>
                                    <option value="PRIVADA" <?php if($tipo=='PRIVADA') echo 'selected="selected"';  ?>>Privada</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <label for="carrera" class="form-label">Nombre de la institución</label>
                            <input type="text" class="form-control" id="txtNombre" name="nombre"  value="<?php echo $nombre; ?>" />
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <label for="carrera" class="form-label">Calle</label>
                                <input type="text" class="form-control" id="txtCalle" name="calle"  value="<?php echo $calle; ?>" />
                            </div>
                            <div class="col-sm-3">
                                <label for="carrera" class="form-label">Número</label>
                                <input type="text" class="form-control" id="txtNum" name="num"  value="<?php echo $num; ?>" />
                            </div>
                            <div class="col-sm-3">
                                <label for="carrera" class="form-label">C.P.</label>
                                <input type="text" class="form-control" id="txtCp" name="cp"  value="<?php echo $cp; ?>" />
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <label for="carrera" class="form-label">Colonia</label>
                            <input type="text" class="form-control" id="txtColonia" name="colonia"  value="<?php echo $colonia; ?>" />
                        </div>

                        

                        <div class="row">
                            <div class="col-sm-6">
                                <label for="carrera" class="form-label">Fecha de conclusión (proporcionar en caso de haber <strong>concluido</strong> en el año 2020 o anterior)</label>
                                <input type="date" class="form-control" id="txtcarrera" name="fechaConclusion" value="<?php echo $fechaConclusion; ?>" />
                            </div>
                            <div class="col-sm-6">
                                <label for="carrera" class="form-label">Promedio</label>
                                <input type="decimal" class="form-control" id="txtcarrera" name="promedio" value="<?php echo $promedio; ?>" />
                            </div>
                        </div>
                        
                        
                        <?php echo $this->include('perfil/botonesguardar'); ?>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    function cambiaEstados() {
        let cmbPais = document.getElementById('cmbPaises' );
        if ( cmbPais.value == 'EXTRANJERO' ) {
            document.getElementById('cmbEstados').value="EXTRANJERO";
            document.getElementById('cmbMunicipios').value="NA";
        }
        else {
            document.getElementById('cmbEstados').value="";
        }
    }

    var cambiaMunicipios = async () => {
        let estado = document.getElementById( 'cmbEstados' ).value;
        const formData = new FormData();

        formData.append('estado', estado);
        document.getElementById('cmbMunicipios').innerHTML = '';
        let response = await fetch('<?php echo base_url("consultas/getmunicipioscct") ?>', {
            method: 'POST',
            body: formData,
        }).then(response => response.json()).then(result => {
                return result;
            }).catch(error => {
                console.error('Error:', error);
            });

        document.getElementById('cmbMunicipios').innerHTML = '';

        for ( let i=0; i<response.length; i++ ) {
            let option = document.createElement('option');
            option.value = response[i];
            option.innerHTML = response[i];
            document.getElementById('cmbMunicipios').appendChild(option);
        }

        let option = document.createElement('option');
        option.value = 'NA';
        option.innerHTML = 'Extranjero';
        document.getElementById('cmbMunicipios').appendChild(option);

        option = document.createElement('option');
        option.value = '';
        option.innerHTML = '';
        document.getElementById('cmbMunicipios').appendChild(option);

        document.getElementById('cmbMunicipios').value = '<?php echo $municipio; ?>';
    }

    var cambiaEscuelas = async () => {
        let mpio = document.getElementById( 'cmbMunicipios' ).value;
        const formData = new FormData();

        formData.append('municipio', mpio);
        document.getElementById('cmbEscuelas').innerHTML = '';
        let response = await fetch('<?php echo base_url("consultas/getccts") ?>', {
            method: 'POST',
            body: formData,
        }).then(response => response.json()).then(result => {
                return result;
            }).catch(error => {
                console.error('Error:', error);
            });
        console.log( response );

        document.getElementById('cmbEscuelas').innerHTML = '';

        let opt = document.createElement('option');
        opt.value = "";
        opt.innerHTML = "OTRA";
        document.getElementById('cmbEscuelas').appendChild(opt);

        for ( let i=0; i<response.length; i++ ) {
            let option = document.createElement('option');
            option.value = response[i].cct + '|' + response[i].nombre + '|' + response[i].calle + '|' + response[i].num + '|' + response[i].colonia + '|' + response[i].cp;
            option.innerHTML = response[i].nombre + ' (' + response[i].colonia + ')';
            document.getElementById('cmbEscuelas').appendChild(option);
        }
    }

    var llenaEscuela = () => {
        let cmbEscuela = document.getElementById( 'cmbEscuelas' );
        let datosEscuela = cmbEscuela.value.split('|');

        document.getElementById('txtCct').value = datosEscuela[0];
        document.getElementById('txtTipo').value = 'PUBLICA';
        document.getElementById('txtNombre').value = datosEscuela[1];
        document.getElementById('txtCalle').value = datosEscuela[2];
        document.getElementById('txtNum').value = datosEscuela[3];
        document.getElementById('txtColonia').value = datosEscuela[4];
        document.getElementById('txtCp').value = datosEscuela[5];
        console.log( datosEscuela );

    }

    cambiaMunicipios();

    
</script>


<?php echo $this->endSection() ?>