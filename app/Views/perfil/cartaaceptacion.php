<div style="margin-top: 35px; margin-left: 40px; margin-right: 40px; font-family: Arial, Helvetica, sans-serif;">
    <div style="margin: 20px;">
        <table width="100%" style="margin-top: 40px;">
            <tr>
                <td >
                    <img src="<?php echo base_url('img/logos/Gobierno.png'); ?>" />
                </td>
                <td >
                    <img src="<?php echo base_url('img/logos/SEP.png'); ?>" />
                </td>
                <td >
                    <img src="<?php echo base_url('img/logos/USEP.png'); ?>" />
                </td>
            </tr>
        </table>
    </div>

    <div style="text-align: center; font-weight: bold; margin-bottom: 20px;">
        CARTA DE ACEPTACIÓN
    </div>

    <div style="text-align: right; margin-bottom: 20px;">
        Puebla, Pue., a 10 de julio de 2021
    </div>

    <div style="margin-bottom: 20px;">
        <strong><?php echo "$nombre $apPaterno $apMaterno"; ?></strong><br />
        Folio: <?php echo $id; ?><br />
        Carrera: <?php echo $carrera; ?> <br />
    </div>

    <div style="margin-bottom: 20px;">
        Me es grato saludarte y al mismo tiempo informarte que, gracias a tu esfuerzo y
        dedicación, el puntaje obtenido en el proceso de admisión 2021 ha sido suficiente para
        obtener un lugar dentro de la Universidad de la Salud del Estado de Puebla.
    </div>

    <div style="text-align: center; font-weight: bold; margin-bottom: 20px;">
        Puntaje: <?php echo number_format( $puntaje, 0, '.', ',') ?>
    </div>

    <div style="margin-bottom: 20px;">
        Has sido aceptado(a) para realizar tus estudios dentro del área de la salud, mismos que
        requieren de total entrega, vocación, sacrificio, honestidad, nobleza y sentido humanista
        desde el primer día de clases.
    </div>

    <div style="margin-bottom: 20px;">
        En USEP potencializamos la formación de nuestros estudiantes al brindarles la
        más alta calidad en docentes, laboratorios, simuladores, instrumentos de enseñanza -
        aprendizaje, convenios colaborativos, becas y recursos tecnológicos, seguidos del puntual
        acompañamiento de toda la planta administrativa que conformamos la Universidad. Con
        ello garantizamos que nuestros estudiantes adquieran conocimientos y competencias que
        les permitan enfrentar los nuevos retos que en materia de salud exige nuestra sociedad.
    </div>

    <div style="margin-bottom: 20px;">
        Ahora debes consultar el “Comunicado con las bases para la inscripción 2021”
        para conocer los requisitos y el procedimiento que deberás cumplir para formalizar tu
        estatus de estudiante.
    </div>

    <div style="text-align: center; font-weight: bold;">
        <p>¡Éxito en esta nueva etapa!</p>
        <p style="margin-bottom: 100px;">
            Atentamente<br />
            “Un pueblo sano, hace una nación vigorosa”
        </p>
        <p>
            Dr. José Hugo Eloy Meléndez Aguilar<br />
            Rector
        </p>
    </div>
</div>

<div style="background-image: url('<?php echo base_url('img/plecas/carta_aceptacion_footer.png'); ?>'); background-size: 100%; height: 185px; background-repeat: no-repeat; background-position: bottom;">
    
</div>