<?php echo $this->extend('plantilla_menu'); ?>

<?php echo $this->section('workarea') ?>


<div>&nbsp;</div>
<?php echo $this->include('perfil/progreso'); ?>
<div>&nbsp;</div>

<div class="container-fluid">
    <div class="row">
        
        <div class="col-sm-12">
            <div class="card">
                
                <div class="card-body">
                    <?php if ( $aceptado === '1' ): ?>
                        <div class="row">
                            <div class="col-sm-9" style="border: 1px solid #cdcdcd;">
                                <?php echo $this->include('perfil/cartaaceptacion.php'); ?>
                            </div>
                            <div class="col-sm-3">
                                <a href="<?php echo base_url('Perfil/CartaAceptacion'); ?>" class="btn btn-secondary" target="_blank">
                                    Imprimir mi carta de aceptación
                                </a><p></p>
                                <p>
                                    Enlaces importantes
                                </p>
                                <ul>
                                    <li>
                                        <a href="https://aplicativos.usep.mx/docs/n/speti/dset/admision2021/USEP%20-%20Comunicado%20de%20Inscripci%C3%B3n%202021.pdf" target="_blank">Comunicado con las bases para la inscripción 2021</a>
                                    </li>
                                    <li>
                                        <a href="https://aplicativos.usep.mx/docs/n/speti/dset/admision2021/Formato%20consentimiento.pdf" target="_blank">Formato de consentimiento para uso de datos personales</a>
                                    </li>
                                    <li>
                                        <a href="https://rl.puebla.gob.mx/" target="_blank">Liga para obtener la orden de cobro por &quot;Cuota Integral de Inscripción&quot;</a>
                                    </li>
                                </ul>
                                <div>
                                    <p>Cita para presentar los documentos</p>
                                    Fecha: <?php echo $inscFecha; ?> <br />
                                    Hora: <?php echo $inscHora; ?> <br />
                                </div>
                            </div>
                        </div>
                        
                    <?php else: ?>
                        <?php echo $this->include('perfil/cartarechazo.php'); ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>

<?php echo $this->endSection() ?>