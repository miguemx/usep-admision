<?php echo $this->extend('plantilla_menu'); ?>

<?php echo $this->section('workarea') ?>



<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                    
                </div>
                <div class="card-body">
                    <div class="alert alert-success">
                        <p>&iexcl;<?php echo ucwords($nombre); ?>!</p>
                        <p>
                            Se ha enviado la liga para continuar tu registro al correo electrónico proporcionado.<br />
                            Si en unos minutos no te ha llegado, te sugerimos revisar tus bandejas de correo no deseado o spam,
                            así como las pestañas de correos informativos, redes sociales o promocionales en el caso de que utilices GMail.
                        </p>
                        <p>Da clic en ella para continuar</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<?php echo $this->endSection() ?>