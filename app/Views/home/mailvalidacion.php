<div style="text-align: center; color: #666;">
    <h3>{{nombre}}</h3>
    

    <p>
        Tus datos de acceso para el portal de aspirantes son:<br />
        <ul>
            <li><strong>CURP:</strong> {{curp}}</li>
            <li><strong>Folio de registro:</strong> {{matricula}}</li>
        </ul>
    </p>

    <p>
        Para completar tu registro, haz clic en la siguiente liga:<br />
        <a href="{{liga}}">Acceso al portal de aspirantes</a>
    </p>

    <p>Es importante que guardes este correo porque es tu llave de acceso.</p>
    
</div>