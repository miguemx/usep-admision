<?php echo $this->extend('plantilla_menu'); ?>

<?php echo $this->section('workarea') ?>



<div class="container-fluid">
    <div class="row">

        <div class="col-sm-6">
            <div class="card">
                <div class="card-header" >
                    <h2>Inicia tu registro aquí</h2>
                </div>
                <div class="card-body">
                    <?php if ( isset($errors) ): ?>
                        <div class="alert alert-danger" role="alert">
                            <?php foreach($errors as $error ) {
                                echo $error.'<br />';
                            } ?>
                        </div>
                    <?php endif; ?>
                    <form id="formregistro" action="<?php echo base_url('Registro') ?>" method="post" onSubmit="onSubmit()">
                        <input type="hidden" value="" name="crt" id="crt" />
                        <div class="row">
                            <div class="col-sm-6">
                                <label for="apellidoPaterno" class="form-label">Apellido Paterno</label>
                                <input type="text" class="form-control" id="apPaterno" name="apPaterno" value="<?php echo $apPaterno; ?>" />
                            </div>

                            <div class="col-sm-6">
                                <label for="apellidoMaterno" class="form-label">Apellido Materno</label>
                                <input type="text" class="form-control" id="apMaterno" name="apMaterno" value="<?php echo $apMaterno; ?>" />
                            </div>
                        </div>
                        
                        <div class="col-sm-12">
                            <label for="nombre" class="form-label">Nombre(s)</label>
                            <input type="text" class="form-control" id="nombre" name="nombre" value="<?php echo $nombre; ?>" />
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                <label for="curp" class="form-label">CURP</label>
                                <input type="text" class="form-control" id="curp" name="curp" value="<?php echo $curp; ?>" />
                            </div>
                            <div class="col-sm-6">
                                <label for="telefono" class="form-label">Teléfono</label>
                                <input type="text" class="form-control" id="telefono" name="telefono" value="<?php echo $telefono; ?>" />
                            </div>
                        </div>

                        <div class="col-sm-12">
                            <label for="correo" class="form-label">Correo Electrónico</label>
                            <input type="text" class="form-control" id="correo" name="correo" value="<?php echo $correo; ?>" />
                        </div>

                        <div class="col-sm-12">
                            <label for="carrera" class="form-label">Opción de Ingreso</label>
                            <select class="form-control" id="carrera" name="carrera">
                                <option value=""></option>
                                <option value="1">LICENCIATURA EN ENFERMERÍA Y OBSTETRICIA</option>
                                <option value="2">LICENCIATURA EN MÉDICO CIRUJANO</option>
                            </select>
                        </div>

                        <div class="form-check col-sm-12" style="margin-top: 15px;">
                            <input class="form-check-input" type="checkbox" value="1" id="checkterminos" name="checkterminos">
                            <label class="form-check-label" for="flexCheckDefault">
                                Estoy de acuerdo con el
                                <a href="https://aplicativos.usep.mx/docs/Aviso_de_privacidad_USEP.pdf" target="_blank">
                                    Aviso de privacidad
                                </a>
                            </label>
                        </div>

                        <div class="row" style="margin-top:10px">
                            <div class="col-sm-6">
                                <div class="g-recaptcha" data-sitekey="6LcPla4aAAAAAPmqbrStrHxp2NLfbvfpzNgZABBi" data-theme="light" data-callback="prepare" id="html_element"></div>
                            </div>
                            <div class="col-sm-6" style="text-align: right;">
                                <button type="submit" class="btn btn-secondary" disabled="disabled" id="btnRegistro">
                                    Registrarse
                                </button> 
                            </div>
                        </div>
                        
                    </form>
                </div>
            </div>
        </div>

        <div class="col-sm-6">
            <div class="card">
                <div class="card-header">
                    <h2>Si ya estás registrado, continua aquí</h2>
                    
                </div>
                <div class="card-body">
                    <?php if ( isset($sesserror) ): ?>
                        <div class="alert alert-danger" role="alert">
                           
                            <?php    echo $sesserror; ?>
                             
                        </div>
                    <?php endif; ?>
                    <form id="formlogin" action="<?php echo base_url('Home/Login') ?>" method="post" >
                        <div class="mb-3">
                            <label for="logincurp" class="form-label">CURP</label>
                            <input type="text" class="form-control" id="logincurp" name="logincurp" aria-describedby="emailHelp">
                        </div>
                        <div class="mb-3">
                            <label for="loginfolio" class="form-label">Folio de registro</label>
                            <input type="password" class="form-control" id="loginfolio" name="loginfolio">
                        </div>

                        <div class="row" style="margin-top:10px">
                            <div class="col-sm-6">
                                <div class="g-recaptcha" data-sitekey="6LcPla4aAAAAAPmqbrStrHxp2NLfbvfpzNgZABBi" data-theme="light" data-callback="prepareLogin" id="captcha_login"></div>
                            </div>
                            <div class="col-sm-6" style="text-align: right;">
                                <button type="submit" class="btn btn-secondary" disabled="disabled" id="btnLogin">
                                    Acceder    
                                </button>
                            </div>
                        </div>

                        
                        
                    </form>
                </div>
            </div>

            <div style="margin-top: 20px; font-size: 16px;">
                <a href="<?php echo base_url('Home/Recuperacion/1') ?>">¿No recibiste tu correo para validar tu registro?</a>
            </div>

            <div style="font-size: 16px;">
                <a href="<?php echo base_url('Home/Recuperacion/2') ?>">¿No conoces tus datos de acceso?</a>
            </div>

            <div class="card" style="margin-top: 25px; font-weight: bold;">
                <div class="card-header">
                    <a href="https://usep.puebla.gob.mx/images/documentos/admision2021/Instructivo_RegistroEnLinea.pdf" target="_blank">Instructivo del registro en línea</a><br />
                    <a href="https://usep.puebla.gob.mx/images/documentos/admision2021/Manual_Digitalizacion.pdf" target="_blank">Manual de digitalización</a><br />
                    <a href="https://usep.puebla.gob.mx/images/documentos/admision2021/Temario_Examen.pdf" target="_blank">Temario del examen</a>
                </div>
            </div>
        </div>

    </div>
</div>

<script type="text/javascript">
    var onloadCallback = function() {
        grecaptcha.render('html_element', {
            'site_key' : '6LcPla4aAAAAAPmqbrStrHxp2NLfbvfpzNgZABBi'
        });

        grecaptcha.render('captcha_login', {
            'site_key' : '6LcPla4aAAAAAPmqbrStrHxp2NLfbvfpzNgZABBi'
        });
    };

    function prepare() {
        document.getElementById('btnRegistro').disabled = false;
        document.getElementById('crt').value = "1";
    }

    function prepareLogin() {
        document.getElementById('btnLogin').disabled = false;
    }
</script>
<script src="https://www.google.com/recaptcha/api.js" async defer></script>
<script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit" async defer></script>



<?php echo $this->endSection() ?>