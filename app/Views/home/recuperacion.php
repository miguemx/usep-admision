<?php echo $this->extend('plantilla_menu'); ?>

<?php echo $this->section('workarea') ?>



<div class="container-fluid">
    <div class="row">

        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                    Por favor proporciona los siguientes datos para realizar la consulta.
                </div>
                <div class="card-body">
                    <?php if ( isset($enlace) ): ?>
                        <div class="alert alert-success" role="alert">
                            Tus datos han sido encontrados.<br />
                            Para validar tu cuenta, deberás hacer clic en el siguiente enlace:<br />
                            <a href="<?php echo $enlace; ?>" class="alert-link">Validar mi cuenta.</a>
                        </div>
                    <?php endif; ?>
                    <?php if ( isset($dfolio) ): ?>
                        <div class="alert alert-success" role="alert">
                            Tus datos han sido encontrados.<br />
                            Tu folio de acceso es: <strong><?php echo $dfolio; ?></strong><br /><br />
                            Recuerda que para acceder a la Plataforma de aspirantes, es necesario que proporciones
                            tu <strong>CURP</strong> y el <strong>folio</strong> que acabas de consultar.
                        </div>
                    <?php endif; ?>
                    <?php if ( isset($error) ): ?>
                        <div class="alert alert-danger" role="alert">
                            <?php    echo $error; ?>
                        </div>
                    <?php endif; ?>
                    <form id="formlogin" action="<?php echo $action; ?>" method="post" >
                        <div class="mb-3">
                            <label for="logincurp" class="form-label">CURP</label>
                            <input type="text" class="form-control" id="logincurp" name="curp" value="<?php echo $curp; ?>">
                        </div>
                        <div class="mb-3">
                            <label for="loginfolio" class="form-label">Apellido paterno</label>
                            <input type="text" class="form-control" id="loginfolio" name="apPaterno" value="<?php echo $apPaterno; ?>">
                        </div>
                        <div class="mb-3">
                            <label for="loginfolio" class="form-label">Telefono</label>
                            <input type="text" class="form-control" id="loginfolio" name="telefono" value="<?php echo $telefono; ?>">
                        </div>

                        <div class="row" style="margin-top:10px">
                            <div class="col-sm-6">
                                <div class="g-recaptcha" data-sitekey="6LcPla4aAAAAAPmqbrStrHxp2NLfbvfpzNgZABBi" data-theme="light" data-callback="prepareLogin" id="captcha_login"></div>
                            </div>
                            <div class="col-sm-6" style="text-align: right;">
                                <button type="submit" class="btn btn-primary" disabled="disabled" name="btbus" value="1" id="btnLogin">
                                    Consultar    
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>
</div>

<script type="text/javascript">
    var onloadCallback = function() {
        grecaptcha.render('html_element', {
            'site_key' : '6LcPla4aAAAAAPmqbrStrHxp2NLfbvfpzNgZABBi'
        });

        grecaptcha.render('captcha_login', {
            'site_key' : '6LcPla4aAAAAAPmqbrStrHxp2NLfbvfpzNgZABBi'
        });
    };

    function prepare() {
        document.getElementById('btnRegistro').disabled = false;
        document.getElementById('crt').value = "1";
    }

    function prepareLogin() {
        document.getElementById('btnLogin').disabled = false;
    }
</script>
<script src="https://www.google.com/recaptcha/api.js" async defer></script>
<script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit" async defer></script>



<?php echo $this->endSection() ?>