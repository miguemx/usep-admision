<div style="text-align: center; color: #666;">
    <h3>{{nombre}}</h3>

    <p>Bienvenido(a) al registro que te permitirá ser candidato(a) para alguno de los programas 
    ofertados por la Universidad de la Salud del Estado de Puebla (USEP).</p>

    <p>Ser un estudiante USEP requiere de todo tu empeño, dedicación y plena convicción 
    para que, al egresar, seas uno de los mejores profesionistas del país.</p>
    
    <p>
    Para continuar da clic en la siguiente liga:<br />
    <a href="{{liga}}">Registro para aspirantes USEP</a>
    </p>
</div>