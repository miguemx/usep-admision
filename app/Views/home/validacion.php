<?php echo $this->extend('plantilla_menu'); ?>

<?php echo $this->section('workarea') ?>



<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                    
                </div>
                <div class="card-body">
                    <?php if( isset($valido) ): ?>
                        <div class="alert alert-success" role="alert">
                            <p>
                            Tu cuenta ha sido validada. <br />
                            Se han enviado al correo electrónico que proporcionaste, los datos de acceso.
                            Una vez que cuentes con ellos, da clic
                            <a href="<?php echo base_url() ?>" class="alert-link">aquí</a>
                            para continuar.
                            </p>
                            <p>
                            En caso de que no hayas recibido tu correo electrónico, proporciona los datos que ingresate
                            en esta 
                            <a href="#" class="alert-link">liga</a>
                            para recuperar tus datos de acceso.
                            </p>
                        </div>
                    <?php else: ?>
                        <div class="alert alert-danger" role="alert">
                            En este momento no podemos validar tu enlace. <br />
                            <?php if(isset($error)) echo $error; ?>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>



<?php echo $this->endSection() ?>