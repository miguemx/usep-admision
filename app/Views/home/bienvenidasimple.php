<?php echo $this->extend('plantilla_menu'); ?>

<?php echo $this->section('workarea') ?>



<div class="container-fluid">
    <div class="row">

        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                    <h2>Si ya estás registrado, continua aquí</h2>
                    
                </div>
                <div class="card-body">
                    <?php if ( isset($sesserror) ): ?>
                        <div class="alert alert-danger" role="alert">
                           
                            <?php    echo $sesserror; ?>
                             
                        </div>
                    <?php endif; ?>
                    <form id="formlogin" action="<?php echo base_url('Home/Login') ?>" method="post" >
                        <div class="mb-3">
                            <label for="logincurp" class="form-label">CURP</label>
                            <input type="text" class="form-control" id="logincurp" name="logincurp" aria-describedby="emailHelp">
                        </div>
                        <div class="mb-3">
                            <label for="loginfolio" class="form-label">Folio de registro</label>
                            <input type="password" class="form-control" id="loginfolio" name="loginfolio">
                        </div>

                        <div class="row" style="margin-top:10px">
                            <div class="col-sm-6">
                                <div class="g-recaptcha" data-sitekey="6LcPla4aAAAAAPmqbrStrHxp2NLfbvfpzNgZABBi" data-theme="light" data-callback="prepareLogin" id="captcha_login"></div>
                            </div>
                            <div class="col-sm-6" style="text-align: right;">
                                <button type="submit" class="btn btn-secondary" disabled="disabled" id="btnLogin">
                                    Acceder    
                                </button>
                            </div>
                        </div>

                        
                        
                    </form>
                </div>
            </div>

            <div style="font-size: 16px;">
                <a href="<?php echo base_url('Home/Recuperacion/2') ?>">¿No conoces tus datos de acceso?</a>
            </div>

            <div class="card" style="margin-top: 25px; font-weight: bold;">
                <div class="card-header">
                    <a href="https://aplicativos.usep.mx/docs/Video_tutorial_para_examen_de_admision_2021.mp4" target="_blank">Video tutorial para el examen de admisión</a><br />
                    <a href="https://usep.puebla.gob.mx/images/documentos/admision2021/Temario_Examen.pdf" target="_blank">Temario del examen</a>
                </div>
            </div>
        </div>

    </div>
</div>

<script type="text/javascript">
    var onloadCallback = function() {
        grecaptcha.render('html_element', {
            'site_key' : '6LcPla4aAAAAAPmqbrStrHxp2NLfbvfpzNgZABBi'
        });

        grecaptcha.render('captcha_login', {
            'site_key' : '6LcPla4aAAAAAPmqbrStrHxp2NLfbvfpzNgZABBi'
        });
    };

    function prepare() {
        document.getElementById('btnRegistro').disabled = false;
        document.getElementById('crt').value = "1";
    }

    function prepareLogin() {
        document.getElementById('btnLogin').disabled = false;
    }
</script>
<script src="https://www.google.com/recaptcha/api.js" async defer></script>
<script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit" async defer></script>



<?php echo $this->endSection() ?>