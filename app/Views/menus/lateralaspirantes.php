
<div class="col-sm-3">
    <div class="card">
        <div class="card-header">
            <strong>Secciones</strong>
        </div>
        <div class="card-body">
        <ul>
            <li>
                
                <a href="<?php echo base_url('Perfil'); ?>">Datos Generales</a>
                <?php if($progreso['captura']['generales']=='1'): ?><img src="<?php echo base_url('icons/check-circle.png') ?>" /><?php endif; ?>
            </li>
            <li>
                <a href="<?php echo base_url('Perfil/Socioeconomicos'); ?>">Datos Socioeconómicos</a>
                <?php if($progreso['captura']['socioeconomicos']=='1'): ?><img src="<?php echo base_url('icons/check-circle.png') ?>" /><?php endif; ?>
            </li>
            <li>
                <a href="<?php echo base_url('Perfil/Salud'); ?>">Datos de Salud</a>
                <?php if($progreso['captura']['salud']=='1'): ?><img src="<?php echo base_url('icons/check-circle.png') ?>" /><?php endif; ?>
            </li>
            <li>
                <a href="<?php echo base_url('Perfil/Procedencia'); ?>">Escuela de Procedencia</a>
                <?php if($progreso['captura']['procedencia']=='1'): ?><img src="<?php echo base_url('icons/check-circle.png') ?>" /><?php endif; ?>
            </li>
            <li>
                <a href="<?php echo base_url('Perfil/Emergencia'); ?>">Conctacto de Emergencia</a>
                <?php if($progreso['captura']['emergencia']=='1'): ?><img src="<?php echo base_url('icons/check-circle.png') ?>" /><?php endif; ?>
            </li>
            <?php if($progreso['global']['captura'] == '1'): ?>
                <li>
                    <a href="<?php echo base_url('Documentos'); ?>">Carga de documentos</a>
                    <?php if($progreso['global']['documentos']=='1'): ?><img src="<?php echo base_url('icons/check-circle.png') ?>" /><?php endif; ?>
                </li>
                
            <?php endif; ?>

            <?php if ( $progreso['global']['status'] == 'CORRECTO' ) { ?>
                <li><a href="#">Ficha de Examen</a> (disponible a partir del 17 de junio)</li><?php
            } ?>
        </ul>
        </div>
    </div>
</div>