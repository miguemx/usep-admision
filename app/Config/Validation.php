<?php

namespace Config;

use CodeIgniter\Validation\CreditCardRules;
use CodeIgniter\Validation\FileRules;
use CodeIgniter\Validation\FormatRules;
use CodeIgniter\Validation\Rules;

class Validation
{
	//--------------------------------------------------------------------
	// Setup
	//--------------------------------------------------------------------

	/**
	 * Stores the classes that contain the
	 * rules that are available.
	 *
	 * @var string[]
	 */
	public $ruleSets = [
		Rules::class,
		FormatRules::class,
		FileRules::class,
		CreditCardRules::class,
	];

	/**
	 * Specifies the views that are used to display the
	 * errors.
	 *
	 * @var array<string, string>
	 */
	public $templates = [
		'list'   => 'CodeIgniter\Validation\Views\list',
		'single' => 'CodeIgniter\Validation\Views\single',
	];

	//--------------------------------------------------------------------
	// Rules
	//--------------------------------------------------------------------

	// -------------------- registro ----------------------------------------------------------------------
	public $registro = [
        'apPaterno'        	=> 'required',
        'apMaterno'        	=> 'required',
		'nombre'        	=> 'required',
		'curp'         		=> 'required|regex_match[/^([A-Z][AEIOUX][A-Z]{2}\d{2}(?:0[1-9]|1[0-2])(?:0[1-9]|[12]\d|3[01])[HM](?:AS|B[CS]|C[CLMSH]|D[FG]|G[TR]|HG|JC|M[CNS]|N[ETL]|OC|PL|Q[TR]|S[PLR]|T[CSL]|VZ|YN|ZS)[B-DF-HJ-NP-TV-Z]{3}[A-Z\d])(\d)$/]',
		'correo'           => 'required|valid_email',
		'telefono'       => 'required|numeric|exact_length[10,12,13]',
		'carrera'        	=> 'required|numeric',
		'crt'        	=> 'required|in_list[1]',
		'checkterminos'        	=> 'required|in_list[1]',
	];
	public $registro_errors = [
		'apPaterno' => [ 
			'required' => 'Por favor proporciona tu apellido paterno.', 
		],
		'apMaterno' => [ 
			'required' => 'Por favor proporciona tu apellido materno.', 
		],
		'nombre' => [ 
			'required' => 'Por favor proporciona tu nombre.', 
		],
		'curp' => [ 
			'required' => 'Por favor proporciona tu CURP.', 
			'regex_match' => 'El formato de tu CURP no es correcto. Verifica que esté en mayúsculas y que sean los 18 caracteres'
		],
        'correo' => [
			'required' => 'Por favor proporciona tu Correo Electrónico.',
            'valid_email' => 'El correo electrónico no parece ser correcto.'
		],
		'telefono' => [
			'required' => 'Por favor proporciona tu número de teléfono.',
            'numeric' => 'El número de teléfono debe contener solo números.',
			'exact_length' => 'Por favor proporciona un número de teléfono válido.'
        ],
		'carrera' => [
			'required' => 'Selecciona una opción de ingreso.',
            'numeric' => 'Selecciona una opción de ingreso.',
        ],
		'crt' => [
			'required' => 'Por favor verifica el valor del captcha',
			'in_list' => 'Por favor verifica el calor del captcha'
		],
		'checkterminos' => [
			'required' => 'Debes aceptar todos los términos de la convocatoria de admisión 2021',
			'in_list' => 'Debes aceptar todos los términos de la convocatoria de admisión 2021'
		]
	];


	public $generales = [
        'calle'        	=> 'required',
        'numero'        	=> 'required',
		'colonia'         		=> 'required',
		'cp'           => 'required|numeric',
		'municipio'       => 'required',
		'estado'        	=> 'required',
		'pais'        	=> 'required',
		'nacimientoFecha'        	=> 'required',
		'nacimientoMunicipio'        	=> 'required',
		'nacimientoEstado'        	=> 'required',
		'nacimientoPais'        	=> 'required',
	];
	public $generales_errors = [
		'calle'        	=> [ 
			'required' => 'Por favor proporciona la calle donde vives.', 
		],
        'numero'        	=> [ 
			'required' => 'Por favor proporciona el número de tu domicilio.', 
		],
		'colonia'         		=> [ 
			'required' => 'Por favor proporciona el nombre de tu colonia.', 
		],
		'cp'           => [ 
			'required' => 'Por favor proporciona tu código postal.', 'numeric' => 'El código postal no es correcto.'
		],
		'municipio'       => [ 
			'required' => 'Por favor proporciona el nombre del municipio donde vives.', 
		],
		'estado'        	=> [ 
			'required' => 'Por favor proporciona el nombre del estado donde vives.', 
		],
		'pais'        	=> [ 
			'required' => 'Por favor proporciona el nombre del país donde vives.', 
		],
		'nacimientoFecha' => [ 
			'required' => 'Por favor proporciona tu fecha de nacimiento.', 
		],
		'nacimientoMunicipio'        	=> [ 
			'required' => 'Por favor proporciona el nombre del municipio donde naciste.', 
		],
		'nacimientoEstado'        	=> [ 
			'required' => 'Por favor proporciona el nombre del estado donde naciste.', 
		],
		'nacimientoPais'        	=> [ 
			'required' => 'Por favor proporciona el nombre del país donde naciste.', 
		],
	];

	public $socioeconomico = [
        'ingreso'        	=> 'required|decimal',
        'estadoCivil'		=> 'required',
		'hijos'		=> 'required|numeric',
		'dependientes'		=> 'required|numeric',
		'mpSoltero'		=> 'required|in_list[0,1]',
		'personasCasa'		=> 'required|numeric',
		'viveEn'		=> 'required',
		'radicar'		=> 'required',
		'trabajaSalud'		=> 'required|in_list[0,1]',
		'comunidadIndigena'		=> 'required|in_list[0,1]',
		'situacionVulnerable'		=> 'required|in_list[0,1]',
		'tieneDesktop'		=> 'required|in_list[0,1]',
		'tieneLaptop'		=> 'required|in_list[0,1]',
		'tieneWebcam'		=> 'required|in_list[0,1]',
		'conseguirCompu'		=> 'required|in_list[0,1]',
		'tieneInternet'		=> 'required|in_list[0,1]',
		'tieneTablet'		=> 'required|in_list[0,1]',
		'tieneSmartphone'		=> 'required|in_list[0,1]',
		'examenOnline'		=> 'required|in_list[0,1]',
	];

	public $socioeconomico_errors = [
        'ingreso'        	=> [ 'required' => 'Por favor proporciona tu ingreso familiar', 'decimal' => 'Por favor expresa el ingreso en números.'],
        'estadoCivil'		=> [ 'required' => 'Por favor proporciona tu estado civil', ],
		'hijos'		=> [ 'required' => 'Por selecciona una opción de número de hijos', 'numeric' => 'El número de hijos no parece ser correcto.'],
		'dependientes'		=> [ 'required' => 'Por favor selecciona una opción de dependientes económicos', 'numeric' => 'El número de dependientes no parece ser correcto.'],
		'mpSoltero'		=> [ 'required' => 'Por favor selecciona si eres madre o padre soltero', 'in_list' => 'Selecciona si eres madre o padre soltero.'],
		'personasCasa'		=> [ 'required' => 'Por favor selecciona cuantas personas viven en tu casa.', 'numeric' => 'El número de personas que viven en tu casa parece no ser correcto.'],
		'viveEn'		=> [ 'required' => 'Por favor indica donde vives', ],
		'radicar'		=> [ 'required' => 'Por favor indica donde pensarías radicar en caso de ser seleccionado y vivir FUERA de la zona metropolitana de Puebla', ],
		'trabajaSalud'		=> [ 'required' => 'Por favor selecciona si trabajas en sector salud', 'in_list' => 'Selecciona si trabajas en sector salud.'],
		'comunidadIndigena'		=> [ 'required' => 'Por favor selecciona si perteneces a una comunidad indigena', 'in_list' => 'Selecciona si perteneces a una comunidad indigena.'],
		'situacionVulnerable'		=> [ 'required' => 'Por favor selecciona si estás en situación vulnerable', 'in_list' => 'Selecciona si estás en situación vulnerable.'],
		'tieneDesktop'		=> [ 'required' => 'Por favor selecciona si tienes computadora de escritorio', 'in_list' => 'Selecciona si tienes computadora de escritorio.'],
		'tieneLaptop'		=> [ 'required' => 'Por favor selecciona si tienes computadora portátil', 'in_list' => 'Selecciona si tienes computadora portátil.'],
		'tieneWebcam'		=> [ 'required' => 'Por favor selecciona si tienes acceso a micrófono y cámara en tu computadora', 'in_list' => 'Selecciona si tienes acceso a micrófono y cámara en tu computadora.'],
		'conseguirCompu'		=> [ 'required' => 'Por favor selecciona si puedes conseguir una computadora para presentar tu examen', 'in_list' => 'Selecciona si puedes conseguir una computadora para presentar tu examen.'],
		'tieneInternet'		=> [ 'required' => 'Por favor selecciona si cuentas con servicio de Internet', 'in_list' => 'Selecciona si cuentas con servicio de Internet.'],
		'tieneTablet'		=> [ 'required' => 'Por favor selecciona si tienes una tableta electrónica', 'in_list' => 'Selecciona si tienes una tableta electrónica.'],
		'tieneSmartphone'		=> [ 'required' => 'Por favor selecciona si tienes un teléfono iteligente', 'in_list' => 'Selecciona si tienes un teléfono iteligente.'],
		'examenOnline'		=> [ 'required' => 'Por favor selecciona tu opción preferida para presentar tu examen', 'in_list' => 'Selecciona tu opción preferida para presentar tu examen.'],
	];

	public $salud = [
        'tipoSangre'        		=> 'required',
		'certificadoDiscapacidad'   => 'required',
		'tieneProtesis'        		=> 'required',
		'servicioMedico'        	=> 'required',
	];

	public $salud_errors = [
        'tipoSangre'        		=> [ 'required' => 'Por favor selecciona tu tipo de sangre.' ],
		'certificadoDiscapacidad'   => [ 'required' => 'Por favor indica si tienes algún certificado de discapacidad.' ],
		'tieneProtesis'        		=> [ 'required' => 'Por favor indica si tienes alguna prótesis.' ],
		'servicioMedico'        	=> [ 'required' => 'Por favor selecciona el servicio médico con el que cuentas.' ],
	];

	public $procedencia = [
        'tipo'        		=> 'required',
		'nombre'        		=> 'required',
		'municipio'        		=> 'required',
		'estado'        		=> 'required',
		'pais'        		=> 'required',
		'promedio'        		=> 'required|greater_than_equal_to[5]|less_than_equal_to[10]',
		
	];

	public $procedencia_errors = [
        'tipo'        		=> [ 'required' => 'Por favor selecciona el tipo de escuela.' ],
		'nombre'   => [ 'required' => 'Por favor proporciona el nombre de tu escuela de procedencia.' ],
		'municipio'        		=> [ 'required' => 'Por favor proporciona el municipio en el que se encuentra tu escuela.' ],
		'estado'        	=> [ 'required' => 'Por favor proporciona el estado en el que se ubica tu escuela.' ],
		'pais'        	=> [ 'required' => 'Por favor selecciona proporciona el pais en el que se encuentra tu escuela.' ],
		'promedio'        	=> [ 
			'required' => 'Por favor indica el promedio que indica tu constancia de estudios.',
			'greater_than_equal_to' => 'Tu promedio no debe ser menor a 5.0.' ,
			'less_than_equal_to' => 'Tu promedio no debe ser mayor a 10.' 
		],
	];

	public $emergencia = [
        'nombre'        		=> 'required',
		'parentesco'        		=> 'required',
		'telefono'        		=> 'required|numeric',
		'correo'        		=> 'required|valid_email',
		
	];

	public $emergencia_errors = [
        'nombre'        		=> [ 'required' => 'Por favor indica el nombre de tu contacto de emergencia.' ],
		'parentesco'   => [ 'required' => 'Por favor proporciona el parentesco que tienes con tu contacto de emergencia.' ],
		'telefono'        		=> [ 'required' => 'Por favor proporciona el número de teléfono.', 'numeric'=>'El número de contacto parece no ser correcto.' ],
		'correo'        	=> [ 'required' => 'Por favor proporciona el correo electrónico.', 'valid_email'=>'La dirección de correo parece no ser correcta.' ],
	];


}
