<?php
namespace App\Models;
use CodeIgniter\Model;

class AspirantesModel extends Model {
    protected $table      = 'prein_aspirantes';
    protected $primaryKey = 'aspirante_id';

    protected $returnType    = 'App\Entities\Aspirante';
    protected $useSoftDeletes = true;

    protected $allowedFields = [
        'aspirante_id','aspirante_carrera','aspirante_ap_paterno','aspirante_carrera','aspirante_ap_materno','aspirante_nombre','aspirante_curp',
        'aspirante_correo','aspirante_telefono','aspirante_calle','aspirante_numero','aspirante_numero_interior','aspirante_cp','aspirante_colonia',
        'aspirante_municipio','aspirante_estado','aspirante_pais','aspirante_nacimiento_fecha','aspirante_nacimiento_municipio','aspirante_nacimiento_estado',
        'aspirante_nacimiento_pais', 'aspirante_enviado', 'aspirante_examen_fecha','aspirante_examen_hora'
    ];

    protected $useTimestamps = true;

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

    protected $db;
    protected $builder;

    public function __construct() {
        parent::__construct();

        $this->db = \Config\Database::connect();
        $this->db = db_connect();
    }

    /**
     * returns last id inserted
     * @return lastID the last ID inserted
     */
    public function lastId() {
        return $this->db->insertID();
    }

    /**
     * busca un registro de aspirante mediante su curp
     * @param curp el correo que se va a buscar
     * @return Aspirante entidad de Aspirante con los datos encontrados
     * @return null si no se encuentra se retorna un valor nulo
     */
    public function findByCurp($curp) {
        $intento = $this->where('aspirante_curp',$curp)->first();
        return $intento;
    }

    /** 
     * busca el siguiente número de matricula basado en la carrera del estudiante
     * @param carrera el objeto de Carrera con los datos completos
     * @return matricula el numero de la matricula generado
     */
    public function calculaSiguienteMatricula($carrera) {
        $last = $this->where('aspirante_carrera', $carrera->id)->orderBy('aspirante_id', 'DESC')->first();
        $matricula = null;
        if ( is_null($last) ) {
            $matricula = '21'.$carrera->numero.'00001';
        }
        else {
            $matricula = intval($last->id) + 1;
            $matricula = $matricula.'';
        }
        return $matricula;
    }

}
