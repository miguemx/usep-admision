<?php
namespace App\Models;
use CodeIgniter\Model;

class SaludModel extends Model {
    protected $table      = 'prein_aspirante_salud';
    protected $primaryKey = 'salud_aspirante';

    protected $returnType    = 'App\Entities\Salud';
    protected $useSoftDeletes = true;

    protected $allowedFields = [
        'salud_aspirante','salud_tipo_sangre','salud_padecimientos','salud_discapacidad','salud_certificado_discapacidad','salud_usa_lentes','salud_tiene_protesis',
        'salud_servicio_medico','salud_enviado',
    ];

    protected $useTimestamps = true;

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

    protected $db;
    protected $builder;

    public function __construct() {
        parent::__construct();

        $this->db = \Config\Database::connect();
        $this->db = db_connect();
    }

    /**
     * returns last id inserted
     * @return lastID the last ID inserted
     */
    public function lastId() {
        return $this->db->insertID();
    }

    
}
