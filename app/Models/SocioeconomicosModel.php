<?php
namespace App\Models;
use CodeIgniter\Model;

class SocioeconomicosModel extends Model {
    protected $table      = 'prein_socioeconomicos';
    protected $primaryKey = 'socio_aspirante';

    protected $returnType    = 'App\Entities\Socioeconomico';
    protected $useSoftDeletes = true;

    protected $allowedFields = [
        'socio_aspirante','socio_estado_civil','socio_hijos','socio_mp_soltero','socio_personas_casa','socio_vive_en','socio_trabaja_salud',
        'socio_comunidad_indigena','socio_situacion_vulnerable','socio_tiene_desktop','socio_tiene_webcam','socio_tiene_laptop','socio_tiene_internet','socio_tiene_tablet',
        'socio_beca','socio_enviado','socio_dependientes','socio_radicar','socio_vulnerable_cual','socio_admision_previo',
        'socio_admision_actual','socio_tiene_smartphone','socio_examen_online', 'socio_ingreso', 'socio_conseguir_compu',
    ];

    protected $useTimestamps = true;

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

    protected $db;
    protected $builder;

    public function __construct() {
        parent::__construct();

        $this->db = \Config\Database::connect();
        $this->db = db_connect();
    }

    /**
     * returns last id inserted
     * @return lastID the last ID inserted
     */
    public function lastId() {
        return $this->db->insertID();
    }

    
}
