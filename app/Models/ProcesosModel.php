<?php
namespace App\Models;
use CodeIgniter\Model;

class ProcesosModel extends Model {
    protected $table      = 'prein_proceso';
    protected $primaryKey = 'proceso_aspirante';

    protected $returnType    = 'App\Entities\Proceso';
    protected $useSoftDeletes = false;

    protected $allowedFields = [
        'proceso_aspirante','proceso_registro','proceso_validacion','proceso_captura','proceso_documentos',
        'proceso_revision','proceso_exito','proceso_declinado','proceso_carta','proceso_rechazado'
    ];

    protected $useTimestamps = false;

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = true;

    protected $db;
    protected $builder;

    public function __construct() {
        parent::__construct();

        $this->db = \Config\Database::connect();
        $this->db = db_connect();
    }

    /**
     * returns last id inserted
     * @return lastID the last ID inserted
     */
    public function lastId() {
        return $this->db->insertID();
    }

    public function encuentra($matricula) {
        $proceso = $this->like( 'proceso_aspirante', $matricula )->find();
        var_dump( $proceso );
        return $proceso;
    }
    
}
