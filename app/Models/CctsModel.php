<?php
namespace App\Models;
use CodeIgniter\Model;

class CctsModel extends Model {
    protected $table      = 'cat_ccts';
    protected $primaryKey = 'cct_cct';

    protected $returnType    = 'App\Entities\Cct';
    protected $useSoftDeletes = false;

    protected $allowedFields = [
        
    ];

    protected $useTimestamps = false;

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = true;

    protected $db;
    protected $builder;

    public function __construct() {
        parent::__construct();

        $this->db = \Config\Database::connect();
        $this->db = db_connect();
    }

    /**
     * obtiene una lista de todos los estados registrados en la bd de los ccts
     * @return estados un array con la lista de los estados
     */
    public function getEstados() {
        $estados = [];
        $this->builder = $this->db->table( $this->table );
        $this->builder->select('cct_estado');
        $this->builder->distinct();
        $this->builder->orderBy('cct_estado ASC');
        $query = $this->builder->get();
        $rows = $query->getResult();
        foreach ( $rows as $row ) {
            $estados[] = $row->cct_estado;
        }
        return $estados;
    }

    /**
     * obtiene una lista de municipios de acuerdo al estado que se proporcione
     * @param estado el nombre del estado a buscar
     * @return municipios la lista de los municipios encontrados
     */
    public function getMunicipios($estado) {

    }

    /**
     * obtiene una lista de las escuelas que se encuentran dentro de un municipio
     * @param municipio el nombre del municipio
     * @return escuelas la lista de las escuelas
     */
    public function getEscuelas($municipio) {

    }
    
}
