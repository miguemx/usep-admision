<?php
namespace App\Models;
use CodeIgniter\Model;

class IntentosModel extends Model {
    protected $table      = 'prein_intentos';
    protected $primaryKey = 'intento_id';

    protected $returnType    = 'App\Entities\Intento';
    protected $useSoftDeletes = true;

    protected $allowedFields = [
        'intento_opcion','intento_ap_paterno','intento_ap_materno','intento_nombre','intento_curp','intento_correo',
        'intento_telefono',
    ];

    protected $useTimestamps = true;

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

    protected $db;
    protected $builder;

    public function __construct() {
        parent::__construct();

        $this->db = \Config\Database::connect();
        $this->db = db_connect();
    }

    /**
     * returns last id inserted
     * @return lastID the last ID inserted
     */
    public function lastId() {
        return $this->db->insertID();
    }

    /**
     * busca un registro de intento mediante su curp
     * @param curp el correo que se va a buscar
     * @return Intento entidad de Intento con los datos encontrados
     * @return null si no se encuentra se retorna un valor nulo
     */
    public function findByCurp($curp) {
        $intento = $this->where('intento_curp',$curp)->first();
        return $intento;
    }
}
