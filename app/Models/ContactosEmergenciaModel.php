<?php
namespace App\Models;
use CodeIgniter\Model;

class ContactosEmergenciaModel extends Model {
    protected $table      = 'prein_contactos_emergencia';
    protected $primaryKey = 'emergencia_aspirante';

    protected $returnType    = 'App\Entities\ContactoEmergencia';
    protected $useSoftDeletes = true;

    protected $allowedFields = [
        'emergencia_aspirante','emergencia_parentesco','emergencia_telefono','emergencia_correo','emergencia_enviado',
        'emergencia_nombre',
    ];

    protected $useTimestamps = true;

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

    protected $db;
    protected $builder;

    public function __construct() {
        parent::__construct();

        $this->db = \Config\Database::connect();
        $this->db = db_connect();
    }

    /**
     * returns last id inserted
     * @return lastID the last ID inserted
     */
    public function lastId() {
        return $this->db->insertID();
    }

    
}
