<?php
namespace App\Models;
use CodeIgniter\Model;

class DocumentosModel extends Model {
    protected $table      = 'prein_documentos';
    protected $primaryKey = 'documento_id';

    protected $returnType    = 'App\Entities\Documento';
    protected $useSoftDeletes = true;

    protected $allowedFields = [
        'documento_aspirante','documento_tipo','documento_fecha','documento_ruta','documento_status',
        'documento_comentarios',
    ];

    protected $useTimestamps = true;

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

    protected $db;
    protected $builder;

    public function __construct() {
        parent::__construct();

        $this->db = \Config\Database::connect();
        $this->db = db_connect();
    }

    /**
     * returns last id inserted
     * @return lastID the last ID inserted
     */
    public function lastId() {
        return $this->db->insertID();
    }

    /**
     * busca un registro de archivo de aspirante con base en su matricula y el tipo de archivo
     */
    public function busca($matricula, $tipo) {
        $documento = $this->where('documento_aspirante', $matricula)->where('documento_tipo', $tipo)->first();
        return $documento;
    }

    /**
     * obtiene todos los archivos de un aspirante 
     */
    public function getArchivos($matricula) {
        $archivos = $this->where('documento_aspirante', $matricula)->findAll();
        $documentos = [];
        foreach ( $archivos as  $archivo ) {
            $tipoDocumentoModel = new TiposDocumentosModel();
            $tipo = $tipoDocumentoModel->find( $archivo->tipo );
            $archivo->tipoText = $tipo->nombre;
            $documentos[] = $archivo;
        }
        return $documentos;
    }
}
