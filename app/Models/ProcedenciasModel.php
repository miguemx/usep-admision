<?php
namespace App\Models;
use CodeIgniter\Model;

class ProcedenciasModel extends Model {
    protected $table      = 'prein_procedencia';
    protected $primaryKey = 'procedencia_aspirante';

    protected $returnType    = 'App\Entities\Procedencia';
    protected $useSoftDeletes = true;

    protected $allowedFields = [
        'procedencia_aspirante','procedencia_tipo','procedencia_cct','procedencia_nombre','procedencia_calle','procedencia_num','procedencia_cp',
        'procedencia_colonia','procedencia_municipio','procedencia_estado','procedencia_pais','procedencia_fecha_conclusion','procedencia_promedio','procedencia_enviado',
    ];

    protected $useTimestamps = true;

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

    protected $db;
    protected $builder;

    public function __construct() {
        parent::__construct();

        $this->db = \Config\Database::connect();
        $this->db = db_connect();
    }

    /**
     * returns last id inserted
     * @return lastID the last ID inserted
     */
    public function lastId() {
        return $this->db->insertID();
    }

    
}
