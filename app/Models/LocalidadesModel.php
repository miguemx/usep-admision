<?php
namespace App\Models;
use CodeIgniter\Model;

class LocalidadesModel extends Model {

    public function __construct() {
        parent::__construct();

        $this->db = \Config\Database::connect();
        $this->db = db_connect();
    }

    /**
     * obtiene un listado de los estados
     */
    public function getEstados() {
        $builder = $this->db->table('estados');
        $query = $builder->get();
        $estados = $query->getResult();
        return $estados;
    }

    /**
     * devuelve la lista de municipios de un determinado estado del cual se proporciona
     * el nombre en lugar del ID
     * @param nombre el nombre del estado
     * @return municipios la lista de los municipios encontrados (vacia si no se encuentra)
     */
    public function getMunicipiosPorNombreEstado($nombre) {
        $municipios = [];
        $builder = $this->db->table( 'estados' );
        $builder->where( 'nombre', $nombre );
        $query = $builder->get();
        $res = $query->getResult();
        if ( count($res) ) {
            $municipios = $this->getMunicipios( $res[0]->id );
        }
        return $municipios;
    }

    /**
     * obtiene la lista de municipios de acuerdo al estado proporcionado
     * @param idEstado el ID del estadoa  buscar sus municipios
     * @return municipios la lista de los municipios encontrados
     */
    public function getMunicipios($idEstado) {
        $builder = $this->db->table('municipios');
        $builder->where( 'estado_id', $idEstado );
        $query = $builder->get();
        $municipios = $query->getResult();
        return $municipios;
    }

    public function getEstadosCct() {
        $builder = $this->db->table( 'cat_ccts' );
        $builder->select( 'cct_estado' );
        $builder->distinct();
        $builder->orderBy( 'cct_estado','ASC' );
        $query = $builder->get();
        $rows = $query->getResult();
        $estados = [];
        foreach ( $rows as $row ) {
            $estados[]  = $row->cct_estado;
        }
        return $estados;
        
    }

    /**
     * 
     */
    public function getMunicipiosCct( $estado ) {
        $builder = $this->db->table( 'cat_ccts' );
        $builder->select( 'cct_municipio' );
        $builder->distinct();
        $builder->where( 'cct_estado', $estado );
        $builder->orderBy( 'cct_municipio','ASC' );
        $query = $builder->get();
        $rows = $query->getResult();
        $municipios = [];
        foreach ( $rows as $row ) {
            $municipios[]  = $row->cct_municipio;
        }
        return $municipios;
    }

    /**
     * 
     */
    public function getCcts( $municipio ) {
        $builder = $this->db->table( 'cat_ccts' );
        $builder->select( 'cct_cct,cct_nombre,cct_tipo,cct_calle,cct_colonia,cct_num,cct_cp' );
        $builder->where( 'cct_municipio', $municipio );
        $builder->where( 'cct_tipo_sep !=', 'ADMINISTRATIVO' );
        $builder->where( 'cct_tipo_sep !=', 'SUPERVISION DE ZONA DE EDUCACION' );
        $builder->where( 'cct_tipo_sep !=', 'EFATURA DE SECTOR' );
        $builder->where( 'cct_tipo_sep !=', 'BIBLIOTECA' );
        $builder->where( 'cct_tipo_sep !=', 'ALMACEN DE LIBROS GRATUITOS' );
        $builder->where( 'cct_tipo_sep !=', 'COORDINACION DE ZONA DE EDUCACION FISICA' );
        $builder->where( 'cct_tipo_sep !=', 'RECTORIA' );
        $builder->where( 'cct_tipo_sep !=', 'BOLSA (EN LIQUIDACION)' );
        $builder->where( 'cct_tipo_sep !=', 'APOYO A LA EDUCACION' );
        $builder->orderBy( 'cct_nombre','ASC' );
        $query = $builder->get();
        $rows = $query->getResult();
        $ccts = [];
        foreach ( $rows as $row ) {
            $ccts[]  = [
                'cct' => $row->cct_cct,
                'nombre' => $row->cct_nombre,
                'tipo' => $row->cct_tipo,
                'calle' => $row->cct_calle,
                'colonia' => $row->cct_colonia,
                'num' => $row->cct_num,
                'cp' => $row->cct_cp,
            ];
        }
        return $ccts;
    }

}