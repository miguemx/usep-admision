<?php
namespace App\Filters;

use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use CodeIgniter\Filters\FilterInterface;

class AspirantesFilter implements FilterInterface {

    public function before( RequestInterface $request, $arguments = null ) {
        $session = \Config\Services::session();
        $curp = $session->get('curp');
        $id = $session->get('id');
        if ( is_null($curp) || is_null($id) )
        {
            $session->destroy();
            return redirect()->to( base_url() );
        }
    }

    public function after( RequestInterface $request, ResponseInterface $response, $arguments = null ) {

    }

}
