<?php
namespace App\Entities;

use CodeIgniter\Entity;

class Salud extends Entity {

    // poner los atributos que deseamos sean visibles en toda la aplicacion
    protected $attributes = [
        'aspirante'                    => null,
        'tipoSangre'               => null,
        'padecimientos'             => null,
        'discapacidad'             => null,
        'certificadoDiscapacidad'                => null,
        'usaLentes'                  => null,
        'tieneProtesis'                => null,
        'servicioMedico'              => null,
        'enviado'                 => null,
        
        'created_at' => null,
        'updated_at' => null,
        'deleted_at' => null,
    ];

    // al hacer el mapeo, se pone como llave el atributo y como valor el campo en la base de datos
    // y no como dice la documentacion de CI que lo menciona alreves.
    protected $datamap = [
        'aspirante'                    => 'salud_aspirante',
        'tipoSangre'               => 'salud_tipo_sangre',
        'padecimientos'             => 'salud_padecimientos',
        'discapacidad'             => 'salud_discapacidad',
        'certificadoDiscapacidad'                => 'salud_certificado_discapacidad',
        'usaLentes'                  => 'salud_usa_lentes',
        'tieneProtesis'                => 'salud_tiene_protesis',
        'servicioMedico'              => 'salud_servicio_medico',
        'enviado'                 => 'salud_enviado',
        
        'created_at' => 'created_at',
        'updated_at' => 'updated_at',
        'deleted_at' => 'deleted_at',
    ];

    protected $casts = [
        'options' => 'array',
                'options_object' => 'json',
                'options_array' => 'json-array'
    ];

}
