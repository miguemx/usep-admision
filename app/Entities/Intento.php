<?php
namespace App\Entities;

use CodeIgniter\Entity;

class Intento extends Entity {

    // poner los atributos que deseamos sean visibles en toda la aplicacion
    protected $attributes = [
        'id'                => null,
        'carrera'            => null,
        'apPaterno'   => null,
        'apMaterno'   => null,
        'nombre'            => null,
        'curp'              => null,
        'correo'            => null,
        'telefono'          => null,
        
        'created_at' => null,
        'updated_at' => null,
        'deleted_at' => null,
    ];

    // al hacer el mapeo, se pone como llave el atributo y como valor el campo en la base de datos
    // y no como dice la documentacion de CI que lo menciona alreves.
    protected $datamap = [
        'id'                => 'intento_id',
        'carrera'            => 'intento_opcion',
        'apPaterno'         => 'intento_ap_paterno',
        'apMaterno'         => 'intento_ap_materno',
        'nombre'            => 'intento_nombre',
        'curp'              => 'intento_curp',
        'correo'            => 'intento_correo',
        'telefono'          => 'intento_telefono',
        
        'created_at' => 'created_at',
        'updated_at' => 'updated_at',
        'deleted_at' => 'deleted_at',
    ];

    protected $casts = [
        'options' => 'array',
                'options_object' => 'json',
                'options_array' => 'json-array'
    ];

}
