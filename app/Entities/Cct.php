<?php
namespace App\Entities;

use CodeIgniter\Entity;

class Cct extends Entity {

    // poner los atributos que deseamos sean visibles en toda la aplicacion
    protected $attributes = [
        'cct'                   => null,
        'nombre'                => null,
        'calle'                 => null,
        'num'                   => null,
        'estado'                => null,
        'municipio'             => null,
        'localidad'             => null,  
        'colonia'               => null,
        'cp'                    => null,
        'tipo'                  => null,
    ];

    // al hacer el mapeo, se pone como llave el atributo y como valor el campo en la base de datos
    // y no como dice la documentacion de CI que lo menciona alreves.
    protected $datamap = [
        'cct'                   => 'cct_cct',
        'nombre'                => 'cct_nombre',
        'calle'                 => 'cct_calle',
        'num'                   => 'cct_num',
        'estado'                => 'cct_estado',
        'municipio'             => 'cct_municipio',
        'localidad'             => 'cct_localidad',  
        'colonia'               => 'cct_colonia',
        'cp'                    => 'cct_cp',
        'tipo'                  => 'cct_tipo',
    ];

    protected $casts = [
        'options' => 'array',
                'options_object' => 'json',
                'options_array' => 'json-array'
    ];

}
