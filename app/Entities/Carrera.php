<?php
namespace App\Entities;

use CodeIgniter\Entity;

class Carrera extends Entity {

    // poner los atributos que deseamos sean visibles en toda la aplicacion
    protected $attributes = [
        'id'                => null,
        'nombre'            => null,
        'numero'   => null,
        'clave'   => null,
    ];

    // al hacer el mapeo, se pone como llave el atributo y como valor el campo en la base de datos
    // y no como dice la documentacion de CI que lo menciona alreves.
    protected $datamap = [
        'id'                => 'carrera_id',
        'nombre'            => 'carrera_nombre',
        'numero'         => 'carrera_numero',
        'clave'         => 'carrera_clave',
    ];

    protected $casts = [
        'options' => 'array',
                'options_object' => 'json',
                'options_array' => 'json-array'
    ];

}
