<?php
namespace App\Entities;

use CodeIgniter\Entity;

class ContactoEmergencia extends Entity {

    // poner los atributos que deseamos sean visibles en toda la aplicacion
    protected $attributes = [
        'aspirante'                    => null,
        'nombre'                    => null,
        'parentesco'               => null,
        'telefono'             => null,
        'correo'             => null,
        'enviado'                => null,
        
        
        'created_at' => null,
        'updated_at' => null,
        'deleted_at' => null,
    ];

    // al hacer el mapeo, se pone como llave el atributo y como valor el campo en la base de datos
    // y no como dice la documentacion de CI que lo menciona alreves.
    protected $datamap = [
        'aspirante'                    => 'emergencia_aspirante',
        'nombre'                    => 'emergencia_nombre',
        'parentesco'               => 'emergencia_parentesco',
        'telefono'             => 'emergencia_telefono',
        'correo'             => 'emergencia_correo',
        'enviado'                => 'emergencia_enviado',
        
        'created_at' => 'created_at',
        'updated_at' => 'updated_at',
        'deleted_at' => 'deleted_at',
    ];

    protected $casts = [
        'options' => 'array',
                'options_object' => 'json',
                'options_array' => 'json-array'
    ];

}