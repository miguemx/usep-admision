<?php
namespace App\Entities;

use CodeIgniter\Entity;

class Procedencia extends Entity {

    // poner los atributos que deseamos sean visibles en toda la aplicacion
    protected $attributes = [
        'aspirante'                    => null,
        'tipo'               => null,
        'cct'             => null,
        'nombre'             => null,
        'calle'                => null,
        'num'                  => null,
        'cp'                => null,
        'colonia'              => null,
        'municipio'                 => null,
        'estado'                => null,
        'pais'        => null,
        'fechaConclusion'                    => null,
        'promedio'               => null,
        'enviado'             => null,
        
        
        'created_at' => null,
        'updated_at' => null,
        'deleted_at' => null,
    ];

    // al hacer el mapeo, se pone como llave el atributo y como valor el campo en la base de datos
    // y no como dice la documentacion de CI que lo menciona alreves.
    protected $datamap = [
        'aspirante'                    => 'procedencia_aspirante',
        'tipo'               => 'procedencia_tipo',
        'cct'             => 'procedencia_cct',
        'nombre'             => 'procedencia_nombre',
        'calle'                => 'procedencia_calle',
        'num'                  => 'procedencia_num',
        'cp'                => 'procedencia_cp',
        'colonia'              => 'procedencia_colonia',
        'municipio'                 => 'procedencia_municipio',
        'estado'                => 'procedencia_estado',
        'pais'        => 'procedencia_pais',
        'fechaConclusion'                    => 'procedencia_fecha_conclusion',
        'promedio'               => 'procedencia_promedio',
        'enviado'             => 'procedencia_enviado',
        
        'created_at' => 'created_at',
        'updated_at' => 'updated_at',
        'deleted_at' => 'deleted_at',
    ];

    protected $casts = [
        'options' => 'array',
                'options_object' => 'json',
                'options_array' => 'json-array'
    ];

}
