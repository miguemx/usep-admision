<?php
namespace App\Entities;

use CodeIgniter\Entity;

class Aspirante extends Entity {

    // poner los atributos que deseamos sean visibles en toda la aplicacion
    protected $attributes = [
        'id'                    => null,
        'carrera'               => null,
        'apPaterno'             => null,
        'apMaterno'             => null,
        'nombre'                => null,
        'curp'                  => null,
        'correo'                => null,
        'telefono'              => null,
        'calle'                 => null,
        'numero'                => null,
        'numeroInterior'        => null,
        'cp'                    => null,
        'colonia'               => null,
        'municipio'             => null,
        'estado'                => null,
        'pais'                  => null,
        'nacimientoFecha'       => null,
        'nacimientoMunicipio'   => null,
        'nacimientoEstado'      => null,
        'nacimientoPais'        => null,
        'enviado'               => null,
        'examenFecha'           => null,
        'examenHora'            => null,
        'califFinal'            => null,
        'resOrden'              => null,
        'aceptado'              => null,

        'inscFecha'             => null,
        'inscHora'              => null,
        
        'created_at' => null,
        'updated_at' => null,
        'deleted_at' => null,
    ];

    // al hacer el mapeo, se pone como llave el atributo y como valor el campo en la base de datos
    // y no como dice la documentacion de CI que lo menciona alreves.
    protected $datamap = [
        'id'                    => 'aspirante_id',
        'carrera'               => 'aspirante_carrera',
        'apPaterno'             => 'aspirante_ap_paterno',
        'apMaterno'             => 'aspirante_ap_materno',
        'nombre'                => 'aspirante_nombre',
        'curp'                  => 'aspirante_curp',
        'correo'                => 'aspirante_correo',
        'telefono'              => 'aspirante_telefono',
        'calle'                 => 'aspirante_calle',
        'numero'                => 'aspirante_numero',
        'numeroInterior'        => 'aspirante_numero_interior',
        'cp'                    => 'aspirante_cp',
        'colonia'               => 'aspirante_colonia',
        'municipio'             => 'aspirante_municipio',
        'estado'                => 'aspirante_estado',
        'pais'                  => 'aspirante_pais',
        'nacimientoFecha'       => 'aspirante_nacimiento_fecha',
        'nacimientoMunicipio'   => 'aspirante_nacimiento_municipio',
        'nacimientoEstado'      => 'aspirante_nacimiento_estado',
        'nacimientoPais'        => 'aspirante_nacimiento_pais',
        'enviado'               => 'aspirante_enviado',
        'examenFecha'           => 'aspirante_examen_fecha',
        'examenHora'            => 'aspirante_examen_hora',

        'califFinal'            => 'aspirante_calif_final',
        'resOrden'              => 'aspirante_res_orden',
        'aceptado'              => 'aspirante_aceptado',

        'inscFecha'             => 'aspirante_insc_fecha',
        'inscHora'              => 'aspirante_insc_hora',
        
        'created_at' => 'created_at',
        'updated_at' => 'updated_at',
        'deleted_at' => 'deleted_at',
    ];

    protected $casts = [
        'options' => 'array',
                'options_object' => 'json',
                'options_array' => 'json-array'
    ];

}
