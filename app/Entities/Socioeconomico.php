<?php
namespace App\Entities;

use CodeIgniter\Entity;

class Socioeconomico extends Entity {

    // poner los atributos que deseamos sean visibles en toda la aplicacion
    protected $attributes = [
        'aspirante'                    => null,
        'estadoCivil'               => null,
        'hijos'             => null,
        'dependientes'             => null,
        'mpSoltero'             => null,
        'personasCasa'                => null,
        'viveEn'                  => null,
        'radicar'                  => null,
        'trabajaSalud'                => null,
        'comunidadIndigena'              => null,
        'situacionVulnerable'                 => null,
        'vulnerableCual'                 => null,
        'admisionPrevio'                 => null,
        'admisionActual'                 => null,
        'tieneDesktop'                => null,
        'tieneWebcam'        => null,
        'tieneLaptop'                    => null,
        'tieneInternet'               => null,
        'tieneTablet'             => null,
        'tieneSmartphone'             => null,
        'beca'                => null,
        'enviado'                  => null,
        'examenOnline'                  => null,
        'ingreso'                  => null,
        'conseguirCompu'                  => null,
        
        
        'created_at' => null,
        'updated_at' => null,
        'deleted_at' => null,
    ];

    // al hacer el mapeo, se pone como llave el atributo y como valor el campo en la base de datos
    // y no como dice la documentacion de CI que lo menciona alreves.
    protected $datamap = [
        'aspirante'                    => 'socio_aspirante',
        'estadoCivil'               => 'socio_estado_civil',
        'ingreso'               => 'socio_ingreso',
        'hijos'             => 'socio_hijos',
        'dependientes'             => 'socio_dependientes',
        'mpSoltero'             => 'socio_mp_soltero',
        'personasCasa'                => 'socio_personas_casa',
        'viveEn'                  => 'socio_vive_en',
        'radicar'                => 'socio_radicar',
        'trabajaSalud'                => 'socio_trabaja_salud',
        'comunidadIndigena'              => 'socio_comunidad_indigena',
        'situacionVulnerable'                 => 'socio_situacion_vulnerable',
        'vulnerableCual'                 => 'socio_vulnerable_cual',
        'admisionPrevio'                 => 'socio_admision_previo',
        'admisionActual'                 => 'socio_admision_actual',
        'tieneDesktop'                => 'socio_tiene_desktop',
        'tieneWebcam'        => 'socio_tiene_webcam',
        'tieneLaptop'                    => 'socio_tiene_laptop',
        'tieneInternet'               => 'socio_tiene_internet',
        'tieneTablet'             => 'socio_tiene_tablet',
        'tieneSmartphone'             => 'socio_tiene_smartphone',
        'beca'                => 'socio_beca',
        'enviado'                  => 'socio_enviado',
        'examenOnline'                  => 'socio_examen_online',
        'conseguirCompu'                  => 'socio_conseguir_compu',
        
        
        'created_at' => 'created_at',
        'updated_at' => 'updated_at',
        'deleted_at' => 'deleted_at',
    ];

    protected $casts = [
        'options' => 'array',
                'options_object' => 'json',
                'options_array' => 'json-array'
    ];

}
