<?php
namespace App\Entities;

use CodeIgniter\Entity;

class Documento extends Entity {

    // poner los atributos que deseamos sean visibles en toda la aplicacion
    protected $attributes = [
        'id'                    => null,
        'aspirante'               => null,
        'tipo'             => null,
        'fecha'             => null,
        'ruta'                => null,
        'status'                  => null,
        'comentarios'                => null,
        
        'created_at' => null,
        'updated_at' => null,
        'deleted_at' => null,
    ];

    // al hacer el mapeo, se pone como llave el atributo y como valor el campo en la base de datos
    // y no como dice la documentacion de CI que lo menciona alreves.
    protected $datamap = [
        'id'                    => 'documento_id',
        'aspirante'               => 'documento_aspirante',
        'tipo'             => 'documento_tipo',
        'fecha'             => 'documento_fecha',
        'ruta'                => 'documento_ruta',
        'status'                  => 'documento_status',
        'comentarios'                => 'documento_comentarios',
        
        'created_at' => 'created_at',
        'updated_at' => 'updated_at',
        'deleted_at' => 'deleted_at',
    ];

    protected $casts = [
        'options' => 'array',
                'options_object' => 'json',
                'options_array' => 'json-array'
    ];

}
